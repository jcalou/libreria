# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.25)
# Database: gestion
# Generation Time: 2016-03-22 20:44:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table caja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `caja`;

CREATE TABLE `caja` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `tipo` varchar(15) COLLATE latin1_spanish_ci DEFAULT NULL,
  `descripcion` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `monto` varchar(15) COLLATE latin1_spanish_ci DEFAULT NULL,
  `saldo` varchar(15) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

LOCK TABLES `caja` WRITE;
/*!40000 ALTER TABLE `caja` DISABLE KEYS */;

INSERT INTO `caja` (`id`, `fecha`, `tipo`, `descripcion`, `monto`, `saldo`)
VALUES
	(1,'2016-03-21 07:30:00','Ajuste','Saldo inicial','0','0'),
	(2,'2016-03-22 08:59:02','Entrada','Venta id:30','34.25','34.25');

/*!40000 ALTER TABLE `caja` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table caja_gasto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `caja_gasto`;

CREATE TABLE `caja_gasto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `rubro` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `subrubro` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `monto` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

LOCK TABLES `caja_gasto` WRITE;
/*!40000 ALTER TABLE `caja_gasto` DISABLE KEYS */;

INSERT INTO `caja_gasto` (`id`, `fecha`, `rubro`, `subrubro`, `descripcion`, `monto`)
VALUES
	(1,'2016-03-10 09:11:09','equipamiento','-','prueba','1232'),
	(2,'2016-03-10 09:11:24','movilidad','repuestos','prueba2','232');

/*!40000 ALTER TABLE `caja_gasto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table caja_gasto_rubro
# ------------------------------------------------------------

DROP TABLE IF EXISTS `caja_gasto_rubro`;

CREATE TABLE `caja_gasto_rubro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombrelargo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

LOCK TABLES `caja_gasto_rubro` WRITE;
/*!40000 ALTER TABLE `caja_gasto_rubro` DISABLE KEYS */;

INSERT INTO `caja_gasto_rubro` (`id`, `nombre`, `nombrelargo`)
VALUES
	(1,'movilidad','Movilidad'),
	(2,'personal','Personal'),
	(3,'equipamiento','Equipamiento'),
	(4,'generales','Gastos generales'),
	(5,'locales','Locales'),
	(7,'impuestos','Impuestos'),
	(10,'adelantos','Adelantos');

/*!40000 ALTER TABLE `caja_gasto_rubro` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table caja_gasto_subrubro
# ------------------------------------------------------------

DROP TABLE IF EXISTS `caja_gasto_subrubro`;

CREATE TABLE `caja_gasto_subrubro` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_rubro` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombrelargo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

LOCK TABLES `caja_gasto_subrubro` WRITE;
/*!40000 ALTER TABLE `caja_gasto_subrubro` DISABLE KEYS */;

INSERT INTO `caja_gasto_subrubro` (`id`, `id_rubro`, `nombre`, `nombrelargo`)
VALUES
	(5,'movilidad','repuestos','Repuestos y arreglos'),
	(6,'movilidad','impuestos','Impuestos y cuotas'),
	(7,'movilidad','combvarios','Combustible varios'),
	(12,'locales','alquiler','Alquiler'),
	(13,'locales','varios','Varios'),
	(15,'personal','terceros','Terceros'),
	(21,'movilidad','otros','Otros');

/*!40000 ALTER TABLE `caja_gasto_subrubro` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `contacto` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `cuit` varchar(50) DEFAULT NULL,
  `observaciones` varchar(200) DEFAULT NULL,
  `fecha_carga` datetime DEFAULT NULL,
  `ultima_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;

INSERT INTO `cliente` (`id`, `nombre`, `direccion`, `telefono`, `contacto`, `email`, `cuit`, `observaciones`, `fecha_carga`, `ultima_edicion`)
VALUES
	(1,'Juan','direccion','tel','contacto','email@mail.com','cuit','observaciones','2016-03-16 09:03:52','2016-03-16 09:26:59');

/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table compra
# ------------------------------------------------------------

DROP TABLE IF EXISTS `compra`;

CREATE TABLE `compra` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `observaciones` varchar(150) DEFAULT NULL,
  `total` varchar(20) DEFAULT NULL,
  `pago_con` int(11) DEFAULT NULL,
  `vuelto` varchar(20) DEFAULT NULL,
  `cuenta_corriente` varchar(100) DEFAULT NULL,
  `efectivo` varchar(20) DEFAULT NULL,
  `fecha_carga` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table compra_detalle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `compra_detalle`;

CREATE TABLE `compra_detalle` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_compra` int(11) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unitario` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table pago
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pago`;

CREATE TABLE `pago` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `monto` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;

INSERT INTO `pago` (`id`, `id_cliente`, `fecha`, `monto`, `comentario`)
VALUES
	(1,1,'2016-03-20 09:00:00','2','prueba'),
	(2,1,'2016-03-20 10:00:00','1','prueba 2'),
	(3,1,'2016-03-09 09:24:24','3','ssssss');

/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pagoproveedores
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pagoproveedores`;

CREATE TABLE `pagoproveedores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_proveedor` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `monto` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

LOCK TABLES `pagoproveedores` WRITE;
/*!40000 ALTER TABLE `pagoproveedores` DISABLE KEYS */;

INSERT INTO `pagoproveedores` (`id`, `id_proveedor`, `fecha`, `monto`, `comentario`)
VALUES
	(1,1,'2016-03-21 19:54:53','10','pago a proveedor');

/*!40000 ALTER TABLE `pagoproveedores` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table producto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `subtipo` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `codigo_de_barra` varchar(100) DEFAULT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `stock_minimo` int(11) DEFAULT NULL,
  `costo` varchar(100) DEFAULT NULL,
  `margen` varchar(100) DEFAULT NULL,
  `precio` varchar(100) DEFAULT NULL,
  `ultima_edicion` datetime DEFAULT NULL,
  `fecha_carga` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;

INSERT INTO `producto` (`id`, `nombre`, `tipo`, `subtipo`, `id_proveedor`, `codigo_de_barra`, `marca`, `descripcion`, `stock`, `stock_minimo`, `costo`, `margen`, `precio`, `ultima_edicion`, `fecha_carga`)
VALUES
	(1,'Lapiz Negro',1,1,1,'98129283928','Faber','HB',90,50,'1','50','1.5','2016-02-23 20:04:13',NULL),
	(2,'Birome',1,2,0,'2093740270','bic','birome',98,20,'1','30','1.30','2016-02-24 15:59:17','2016-02-23 19:56:03');

/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table proveedor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `contacto` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `cuit` varchar(50) DEFAULT NULL,
  `observaciones` varchar(200) DEFAULT NULL,
  `fecha_carga` datetime DEFAULT NULL,
  `ultima_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;

INSERT INTO `proveedor` (`id`, `nombre`, `direccion`, `telefono`, `contacto`, `email`, `cuit`, `observaciones`, `fecha_carga`, `ultima_edicion`)
VALUES
	(1,'Proveedor 1','san martin 222','15747474747','Pepe','pepe@mail.com','30-333333333-3','Proveedor de prueba','2016-02-23 20:03:35','2016-03-16 09:03:34'),
	(2,'Juan','test','23232','juan','juan@mail.com','987597859','0986ughjbou go iuh','2016-03-16 09:03:51','2016-03-16 09:03:55');

/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subtipo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subtipo`;

CREATE TABLE `subtipo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_tipo` int(11) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `subtipo` WRITE;
/*!40000 ALTER TABLE `subtipo` DISABLE KEYS */;

INSERT INTO `subtipo` (`id`, `id_tipo`, `nombre`)
VALUES
	(1,1,'Lapiz'),
	(2,1,'Marcador'),
	(3,1,'Mochila'),
	(4,2,'Subrubro 2.1'),
	(5,2,'Subrubro 2.2');

/*!40000 ALTER TABLE `subtipo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tipo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipo`;

CREATE TABLE `tipo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tipo` WRITE;
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;

INSERT INTO `tipo` (`id`, `nombre`)
VALUES
	(1,'Libreria'),
	(2,'Rubro 2'),
	(3,'Rubro 3'),
	(4,'Rubro 4');

/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table venta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `venta`;

CREATE TABLE `venta` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `observaciones` varchar(150) DEFAULT NULL,
  `total` varchar(20) DEFAULT NULL,
  `pago_con` int(11) DEFAULT NULL,
  `vuelto` varchar(20) DEFAULT NULL,
  `cuenta_corriente` varchar(100) DEFAULT NULL,
  `efectivo` varchar(20) DEFAULT NULL,
  `fecha_carga` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;

INSERT INTO `venta` (`id`, `observaciones`, `total`, `pago_con`, `vuelto`, `cuenta_corriente`, `efectivo`, `fecha_carga`)
VALUES
	(1,'',' 14.90',29,'14.10','',NULL,'2016-03-10 19:06:32'),
	(2,'',' 1.50',2,'0.50','',NULL,'2016-03-16 10:20:15'),
	(3,'test',' 1.50',4,'2.50','',NULL,'2016-03-16 10:21:22'),
	(4,'saaw',' 11.20',22,'10.80','1',NULL,'2016-03-16 10:23:17'),
	(5,'pago en efectivo',' 6.00',10,'4.00','1','efectivo','2016-03-22 09:52:11'),
	(6,'ssss',' 2.80',10,'7.20','-','','2016-03-22 09:54:58'),
	(7,'',' 6.00',10,'4.00','-','','2016-03-22 10:58:44');

/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table venta_detalle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `venta_detalle`;

CREATE TABLE `venta_detalle` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unitario` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `venta_detalle` WRITE;
/*!40000 ALTER TABLE `venta_detalle` DISABLE KEYS */;

INSERT INTO `venta_detalle` (`id`, `id_venta`, `id_producto`, `cantidad`, `precio_unitario`)
VALUES
	(1,1,1,3,'$1.50'),
	(2,1,2,8,'$1.30'),
	(3,2,1,1,'$1.50'),
	(4,3,1,1,'$1.50'),
	(5,4,1,4,'$1.50'),
	(6,4,2,4,'$1.30'),
	(7,5,1,4,'$1.50'),
	(8,6,1,1,'$1.50'),
	(9,6,2,1,'$1.30'),
	(10,7,1,4,'$1.50');

/*!40000 ALTER TABLE `venta_detalle` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
