<?php
class venta_model extends CI_Model {
  var $id = '';
  var $observaciones = '';
  var $total = '';
  var $pago_con = '';
  var $vuelto = '';
  var $cuenta_corriente = '';
  var $efectivo = '';
  var $fecha_carga = '';

  var $id_producto = '';
  var $cantidad = '';
  var $precio_unitario = '';

  public function __construct() {
    parent::__construct();
  }

  public function get_ventas(){
    $query = "SELECT * from venta order by id desc";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  public function buscar_ventas($q) {
    $query = "SELECT * from venta WHERE cuenta_corriente LIKE '%" . $q . "%' order by id desc";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  public function buscar_ventas_cc($q) {
    $query = "SELECT * from venta WHERE cuenta_corriente = '" . $q . "' AND efectivo = 'cuentacorriente' order by id desc";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getAllEgresos($limit, $offset) {
    $query = "SELECT * from venta WHERE total < 0 ORDER by fecha_carga DESC LIMIT $limit OFFSET $offset";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getAllEgresosCount() {
    $query = "SELECT count(*) FROM venta WHERE total < 0";
    $q = mysql_query($query);
    $count = mysql_fetch_row($q);
    return $count[0];
  }

  function agregarEgreso() {
    $query = "INSERT INTO venta
      (observaciones,total,fecha_carga)
      VALUES
      ('$this->observaciones', '$this->total', now())";
    mysql_query($query);
    $ret = mysql_insert_id();
    return $ret;
  }

  function eliminarEgreso($id) {
    $query = "DELETE FROM venta WHERE id = $id";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getAllCount() {
    $query = "SELECT count(*) FROM venta";
    $q = $this->db->query($query);
    $count = mysql_fetch_row($q);
    return $count[0];
  }

  function getAll($q, $limit, $offset) {
    if ($q == "_all") {
      $query = "SELECT * FROM venta ORDER BY id desc LIMIT $limit OFFSET $offset";
    }else{
      $query = "SELECT * FROM venta WHERE fecha_carga LIKE '%".$q."%' ORDER BY id desc";
    }
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getId($id) {
    $query = "SELECT v.*, c.nombre as nombre_cliente FROM venta v join cliente c on v.cuenta_corriente = c.id WHERE v.id = $id";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getIdDetalle($id_venta) {
    $query = "SELECT v.*,p.nombre as nombre_producto FROM venta_detalle v LEFT JOIN producto p ON v.id_producto = p.id WHERE id_venta = $id_venta";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  public function agregar_venta() {
    $this->db->insert('venta',array(
        'observaciones'=> $this->observaciones,
        'total'=> $this->total,
        'pago_con'=> $this->pago_con,
        'vuelto'=> $this->vuelto,
        'cuenta_corriente'=> $this->cuenta_corriente,
        'efectivo'=> $this->efectivo,
        'fecha_carga'=> $this->fecha_carga
    ));
    $insert_id = $this->db->insert_id();

    return $insert_id;
  }

  public function eliminar_venta($id) {
    $this->db->delete('venta', array('id' => $id));
  }

  public function agregar_venta_detalle() {
    $this->db->insert('venta_detalle',array(
        'id_venta'=> $this->id_venta,
        'id_producto'=> $this->id_producto,
        'cantidad'=> $this->cantidad,
        'precio_unitario'=> $this->precio_unitario
    ));
    $insert_id = $this->db->insert_id();

    return $insert_id;
  }

  public function eliminar_venta_detalle($id) {
    $this->db->delete('venta_detalle', array('id' => $id));
  }

}
