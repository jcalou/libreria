<?php
class pagoproveedores_model extends CI_Model
{
    var $id = '';
    var $id_proveedor = '';
    var $fecha = '';
    var $monto = '';
    var $comentario = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_pagos()
    {
        $query = "SELECT * from pagoproveedores";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_pago($id)
    {
        $query = "SELECT * from pagoproveedores WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function buscar_pagos($q)
    {
        $query = "SELECT * from pagoproveedores WHERE id_proveedor LIKE '%" . $q . "%'";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_pago()
    {
        $this->db->insert('pagoproveedores',array(
            'id_proveedor'=> $this->id_proveedor,
            'fecha'=> $this->fecha,
            'monto'=> $this->monto,
            'comentario'=> $this->comentario
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_pago($id)
    {
        $this->db->where('id', $id);
        $this->db->update('pagoproveedores',array(
            'id_proveedor'=> $this->id_proveedor,
            'fecha'=> $this->fecha,
            'monto'=> $this->monto,
            'comentario'=> $this->comentario
        ));
    }

    public function eliminar_pago($id)
    {
        $this->db->delete('pagoproveedores', array('id' => $id));
    }


}

