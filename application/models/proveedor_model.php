<?php
class proveedor_model extends CI_Model
{
    var $id = '';
    var $nombre = '';
    var $direccion = '';
    var $telefono = '';
    var $contacto = '';
    var $email = '';
    var $cuit = '';
    var $observaciones = '';
    var $fecha_carga = '';
    var $ultima_edicion = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_proveedores()
    {
        $query = "SELECT * from proveedor";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_proveedores_all()
    {
        $query = "SELECT * from proveedor ";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_proveedores_count()
    {
        $query = "SELECT count(*) as c from proveedor";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_proveedor($id)
    {
        $query = "SELECT * from proveedor WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function buscar_proveedores($q)
    {
        $query = "SELECT * from proveedor WHERE nombre LIKE '%" . $q . "%'";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    function getName($id) {
      $query = "SELECT * from proveedor WHERE id =" . $id;
      $sql = $this->db->query($query);
      return $sql->result();
    }

    public function agregar_proveedor()
    {
        $this->db->insert('proveedor',array(
            'nombre'=> $this->nombre,
            'direccion'=> $this->direccion,
            'telefono'=> $this->telefono,
            'contacto'=> $this->contacto,
            'email'=> $this->email,
            'cuit'=> $this->cuit,
            'observaciones'=> $this->observaciones,
            'fecha_carga'=> $this->fecha_carga,
            'ultima_edicion'=> $this->ultima_edicion
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_proveedor($id)
    {
        $this->db->where('id', $id);
        $this->db->update('proveedor',array(
            'nombre'=> $this->nombre,
            'direccion'=> $this->direccion,
            'telefono'=> $this->telefono,
            'contacto'=> $this->contacto,
            'email'=> $this->email,
            'cuit'=> $this->cuit,
            'observaciones'=> $this->observaciones,
            'ultima_edicion'=> $this->ultima_edicion
        ));
    }

    public function eliminar_proveedor($id)
    {
        $this->db->delete('proveedor', array('id' => $id));
    }


}

