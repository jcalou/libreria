<?php
class cliente_model extends CI_Model
{
    var $id = '';
    var $nombre = '';
    var $direccion = '';
    var $telefono = '';
    var $contacto = '';
    var $email = '';
    var $cuit = '';
    var $observaciones = '';
    var $fecha_carga = '';
    var $ultima_edicion = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_clientes()
    {
        $query = "SELECT * from cliente";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_clientes_all()
    {
        $query = "SELECT * from cliente ";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_clientes_count()
    {
        $query = "SELECT count(*) as c from cliente";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_cliente($id)
    {
        $query = "SELECT * from cliente WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function buscar_clientes($q)
    {
        $query = "SELECT * from cliente WHERE nombre LIKE '%" . $q . "%'";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    function getName($id) {
      $query = "SELECT * from cliente WHERE id =" . $id;
      $sql = $this->db->query($query);
      return $sql->result();
    }

    public function agregar_cliente()
    {
        $this->db->insert('cliente',array(
            'nombre'=> $this->nombre,
            'direccion'=> $this->direccion,
            'telefono'=> $this->telefono,
            'contacto'=> $this->contacto,
            'email'=> $this->email,
            'cuit'=> $this->cuit,
            'observaciones'=> $this->observaciones,
            'fecha_carga'=> $this->fecha_carga,
            'ultima_edicion'=> $this->ultima_edicion
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_cliente($id)
    {
        $this->db->where('id', $id);
        $this->db->update('cliente',array(
            'nombre'=> $this->nombre,
            'direccion'=> $this->direccion,
            'telefono'=> $this->telefono,
            'contacto'=> $this->contacto,
            'email'=> $this->email,
            'cuit'=> $this->cuit,
            'observaciones'=> $this->observaciones,
            'ultima_edicion'=> $this->ultima_edicion
        ));
    }

    public function eliminar_cliente($id)
    {
        $this->db->delete('cliente', array('id' => $id));
    }


}

