<?php
class pago_model extends CI_Model
{
    var $id = '';
    var $id_cliente = '';
    var $fecha = '';
    var $monto = '';
    var $comentario = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_pagos()
    {
        $query = "SELECT * from pago";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_pago($id)
    {
        $query = "SELECT * from pago WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function buscar_pagos($q)
    {
        $query = "SELECT * from pago WHERE id_cliente LIKE '%" . $q . "%'";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_pago()
    {
        $this->db->insert('pago',array(
            'id_cliente'=> $this->id_cliente,
            'fecha'=> $this->fecha,
            'monto'=> $this->monto,
            'comentario'=> $this->comentario
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_pago($id)
    {
        $this->db->where('id', $id);
        $this->db->update('pago',array(
            'id_cliente'=> $this->id_cliente,
            'fecha'=> $this->fecha,
            'monto'=> $this->monto,
            'comentario'=> $this->comentario
        ));
    }

    public function eliminar_pago($id)
    {
        $this->db->delete('pago', array('id' => $id));
    }


}

