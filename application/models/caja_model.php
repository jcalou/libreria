<?php
class caja_model extends CI_Model
{
    //caja
    var $fecha = '';
    var $tipo = ''; //entrada - salida - ajuste
    var $descripcion = '';
    var $monto = '';
    var $saldo = '';

    // gasto
    var $gasto_id = '';
    var $gasto_fecha = '';
    var $gasto_rubro = '';
    var $gasto_subrubro = '';
    var $gasto_descripcion = '';
    var $gasto_monto = '';


    public function __construct()
    {
        parent::__construct();
    }

    public function get_gastos($limit, $offset)
    {
        $query = "SELECT * from caja_gasto ORDER BY fecha desc LIMIT $limit OFFSET $offset";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_gastos_count()
    {
        $query = "SELECT count(*) as c from caja_gasto";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_gastos_reporte($desde,$hasta,$rubro,$subrubro)
    {
        $query = "SELECT * from caja_gasto ";
        $query .= "WHERE (1=1) ";
        if ($desde!="-"){
            $query .= " AND (fecha>='$desde 00:00:00') ";
        }
        if ($hasta!="-"){
            $query .= " AND (fecha<='$hasta 23:59:59') ";
        }
        if ($rubro!="-"){
            $query .= " AND (rubro='$rubro') ";
        }
        if ($subrubro!="-"){
            $query .= " AND (subrubro='$subrubro') ";
        }
        $query .= " ORDER BY fecha desc";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja_reporte($desde,$hasta)
    {
        $query = "SELECT * from caja ";
        $query .= "WHERE (1=1) ";
        if ($desde!="-"){
            $query .= " AND (fecha>='$desde 00:00:00') ";
        }
        if ($hasta!="-"){
            $query .= " AND (fecha<='$hasta 23:59:59') ";
        }
        $query .= " ORDER BY id desc";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_gastos_informe($desde,$hasta)
    {
        $query = "SELECT * from caja_gasto ";
        $query .= "WHERE (1=1) ";
        if ($desde!="-"){
            $query .= " AND (fecha>='$desde 00:00:00') ";
        }
        if ($hasta!="-"){
            $query .= " AND (fecha<='$hasta 23:59:59') ";
        }
        $query .= " ORDER BY rubro,fecha asc";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_gastos_informe_totales($desde,$hasta)
    {
        $query = "SELECT * from caja_gasto ";
        $query .= "WHERE (1=1) ";
        if ($desde!="-"){
            $query .= " AND (fecha>='$desde 00:00:00') ";
        }
        if ($hasta!="-"){
            $query .= " AND (fecha<='$hasta 23:59:59') ";
        }
        $query .= " ORDER BY rubro,subrubro,fecha asc";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_gasto($id)
    {
        $query = "SELECT * from caja_gasto WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_gasto()
    {
        $this->db->insert('caja_gasto',array(
            'id'=> $this->gasto_id,
            'fecha'=> $this->gasto_fecha,
            'rubro'=> $this->gasto_rubro,
            'subrubro'=> $this->gasto_subrubro,
            'descripcion'=> $this->gasto_descripcion,
            'monto'=> $this->gasto_monto
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_gasto($id)
    {
        $this->db->where('id', $id);
        $this->db->update('caja_gasto',array(
            'id'=> $this->gasto_id,
            'fecha'=> $this->gasto_fecha,
            'rubro'=> $this->gasto_rubro,
            'subrubro'=> $this->gasto_subrubro,
            'descripcion'=> $this->gasto_descripcion,
            'monto'=> $this->gasto_monto
        ));
    }

    public function eliminar_gasto($id)
    {
        $this->db->delete('caja_gasto', array('id' => $id));
    }

    public function get_rubros()
    {
        $query = "SELECT * from caja_gasto_rubro";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_subrubros($id_rubro)
    {
        $query = "SELECT * from caja_gasto_subrubro WHERE id_rubro = '".$id_rubro."'";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_cajas($limit, $offset)
    {
        $query = "SELECT * from caja ORDER BY fecha desc LIMIT $limit OFFSET $offset";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja_diaria()
    {
        $query = "SELECT * from caja WHERE DATE(fecha) = CURDATE() ORDER BY fecha desc";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja_diaria_debito()
    {
        $query = "SELECT SUM(total) as val from venta WHERE efectivo = 'debito' AND DATE(fecha_carga) = CURDATE()";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja_diaria_credito()
    {
        $query = "SELECT SUM(total) as val from venta WHERE efectivo = 'credito' AND DATE(fecha_carga) = CURDATE()";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja_diaria_cheque()
    {
        $query = "SELECT SUM(total) as val from venta WHERE efectivo = 'cheque' AND DATE(fecha_carga) = CURDATE()";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja_diaria_debito_dia($dia)
    {
        $query = "SELECT SUM(total) as val from venta
        WHERE efectivo = 'debito'
        AND (fecha_carga>='$dia 00:00:00')
        AND (fecha_carga<='$dia 23:59:59') ";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja_diaria_credito_dia($dia)
    {
        $query = "SELECT SUM(total) as val from venta
        WHERE efectivo = 'credito'
        AND (fecha_carga>='$dia 00:00:00')
        AND (fecha_carga<='$dia 23:59:59') ";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja_diaria_cheque_dia($dia)
    {
        $query = "SELECT SUM(total) as val from venta
        WHERE efectivo = 'cheque'
        AND (fecha_carga>='$dia 00:00:00')
        AND (fecha_carga<='$dia 23:59:59') ";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_saldo()
    {
        $query = "SELECT * from caja ORDER BY fecha desc LIMIT 1";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_cajas_count()
    {
        $query = "SELECT count(*) as c from caja";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_caja($id)
    {
        $query = "SELECT * from caja WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_caja()
    {
        $this->db->insert('caja',array(
            'fecha'=> $this->fecha,
            'tipo'=> $this->tipo,
            'descripcion'=> $this->descripcion,
            'monto'=> $this->monto,
            'saldo'=> $this->saldo
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function getRubroName($rubro) {
      $query = "SELECT * from caja_gasto_rubro WHERE id =" . $rubro;
      $sql = $this->db->query($query);
      return $sql->result();
    }

    public function getSubRubroName($subrubro) {
      $query = "SELECT * from caja_gasto_subrubro WHERE id =" . $subrubro;
      $sql = $this->db->query($query);
      return $sql->result();
    }

    public function get_caja_cierre() {
      $query = "SELECT * from caja WHERE DATE(fecha) = CURDATE() ORDER BY fecha ASC";
      $sql = $this->db->query($query);
      return $sql->result();
    }
}
