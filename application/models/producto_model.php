<?php
class producto_model extends CI_Model {
  var $id = '';
  var $codigo = '';
  var $nombre = '';
  var $tipo = '';
  var $subtipo = '';
  var $id_provedoor = '';
  var $codigo_de_barra = '';
  var $marca = '';
  var $descripcion = '';
  var $stock = '';
  var $stock_minimo = '';
  var $siniva = '';
  var $iva = '';
  var $costo = '';
  var $margen = '';
  var $precio = '';
  var $fecha_carga = '';
  var $ultima_edicion = '';

  public function __construct() {
    parent::__construct();
  }

  function getAllCount() {
    $query = "SELECT count(*) as c FROM producto";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getAll($q, $limit, $offset) {
    if ($q == "_all") {
      $query = "SELECT p.*, e.nombre as e_nombre, e.id as e_id FROM producto p LEFT JOIN proveedor e ON p.id_proveedor = e.id LIMIT $limit OFFSET $offset";
    }else{
      $query = "SELECT p.*, e.nombre as e_nombre, e.id as e_id FROM producto p LEFT JOIN proveedor e ON p.id_proveedor = e.id WHERE p.nombre LIKE '%".$q."%' OR p.codigo_de_barra LIKE '%".$q."%'";
    }
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getStockMinimo() {
    $query = "SELECT p.*, e.nombre as e_nombre, e.id as e_id
              FROM producto p
              LEFT JOIN proveedor e ON p.id_proveedor = e.id
              WHERE p.stock < p.stock_minimo ";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getStockNegativo() {
    $query = "SELECT p.*, e.nombre as e_nombre, e.id as e_id
              FROM producto p
              LEFT JOIN proveedor e ON p.id_proveedor = e.id
              WHERE p.stock < 0 ";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getId($id) {
    $query = "SELECT * FROM producto WHERE id = $id";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  public function agregar_producto() {
    $this->db->insert('producto',array(
      'codigo'=> $this->codigo,
      'nombre'=> $this->nombre,
      'tipo'=> $this->tipo,
      'subtipo'=> $this->subtipo,
      'id_proveedor'=> $this->id_proveedor,
      'codigo_de_barra'=> $this->codigo_de_barra,
      'marca'=> $this->marca,
      'descripcion'=> $this->descripcion,
      'stock'=> $this->stock,
      'stock_minimo'=> $this->stock_minimo,
      'siniva'=> $this->siniva,
      'iva'=> $this->iva,
      'costo'=> $this->costo,
      'margen'=> $this->margen,
      'precio'=> $this->precio,
      'fecha_carga'=> $this->fecha_carga,
      'ultima_edicion'=> $this->ultima_edicion
    ));
    $insert_id = $this->db->insert_id();

    return $insert_id;
  }

  public function editar_producto($id) {
    $this->db->where('id', $id);
    $this->db->update('producto',array(
      'codigo'=> $this->codigo,
      'nombre'=> $this->nombre,
      'tipo'=> $this->tipo,
      'subtipo'=> $this->subtipo,
      'id_proveedor'=> $this->id_proveedor,
      'codigo_de_barra'=> $this->codigo_de_barra,
      'marca'=> $this->marca,
      'descripcion'=> $this->descripcion,
      'stock'=> $this->stock,
      'stock_minimo'=> $this->stock_minimo,
      'siniva'=> $this->siniva,
      'iva'=> $this->iva,
      'costo'=> $this->costo,
      'margen'=> $this->margen,
      'precio'=> $this->precio,
      'ultima_edicion'=> $this->ultima_edicion
    ));
  }

  function actualizarStock($id,$stock) {
    $query = "UPDATE producto SET stock = $stock WHERE id = $id";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function eliminar_producto($id) {
    $this->db->delete('producto', array('id' => $id));
  }

  function getMarcas() {
    $query = "SELECT DISTINCT marca, count(id) as c FROM producto WHERE marca <> '' GROUP BY marca ORDER BY marca";
    $ret = mysql_query($query);
    return $ret;
  }

  function getTipos() {
    $query = "SELECT DISTINCT tipo, count(id) as c FROM producto WHERE tipo <> '' GROUP BY tipo ORDER BY tipo";
    $ret = mysql_query($query);
    return $ret;
  }

  function getProductosMarca($marca) {
    $query = "SELECT * FROM producto WHERE marca = '$marca'";
    $ret = mysql_query($query);
    return $ret;
  }

  function getTipoName($tipo) {
    $query = "SELECT * from tipo WHERE id =" . $tipo;
    $sql = $this->db->query($query);
    return $sql->result();
  }

  function getSubTipoName($subtipo) {
    $query = "SELECT * from subtipo WHERE id =" . $subtipo;
    $sql = $this->db->query($query);
    return $sql->result();
  }

  public function get_rubros(){
    $query = "SELECT * from tipo";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  public function get_subrubros($id_rubro) {
    $query = "SELECT * from subtipo WHERE id_tipo = '".$id_rubro."'";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  public function redondearprecios() {
    $query = "UPDATE producto SET precio = ROUND(precio) where precio > 2";
    $sql = $this->db->query($query);
    return true;
  }

  public function get_productos_import(){
    $query = "SELECT * from importar_producto";
    $sql = $this->db->query($query);
    return $sql->result();
  }

  public function get_producto_import($cod) {
    $query = "SELECT * from producto WHERE codigo='".$cod."'";
    $sql = $this->db->query($query);
    return $sql->result();
  }
}
