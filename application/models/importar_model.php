<?php
class importar_model extends CI_Model
{
    var  $p_codigo = "";
    var  $p_descripcion = "";
    var  $p_adicional = "";
    var  $p_cantidad = "";
    var  $p_precio = "";
    var  $p_iva = "";
    var  $p_final = "";
    var  $p_check = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function limpiar_importar_productos()
    {
        $query = "TRUNCATE importar_producto";
        $sql = $this->db->query($query);
        return true;
    }

    public function agregar_producto()
    {
        $this->db->insert('importar_producto',array(
            'codigo'=> $this->p_codigo,
            'descripcion'=> $this->p_descripcion,
            'adicional'=> $this->p_adicional,
            'cantidad'=> $this->p_cantidad,
            'precio'=> $this->p_precio,
            'iva'=> $this->p_iva,
            'final'=> $this->p_final,
            'check'=> $this->p_check
        ));

        return true;
    }

}
