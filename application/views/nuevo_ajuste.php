<div class=" content row">
  <div class="large-12 columns">
    <h3>Cargar ajuste de caja</h3>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <form action="<?=base_url('caja/ajuste');?>" method="POST" id="theform">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <div class="large-6 columns">
          <label>Nuevo saldo
            <input type="text" name="nuevosaldo" value='' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Comentario
            <input type="text" name="comentario" value='' />
          </label>
        </div>
      </div>

      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Guardar' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>
