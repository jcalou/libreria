<div class="content row">
  <form action="#" method="GET">
    <div class="large-4 columns">
      <h3>Listado de Ventas</h3>
    </div>
    <div class="large-4 columns">
      &nbsp;
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('venta/ingresar');?>" class="button postfix">Ingresar nueva venta</a>
    </div>
  </form>
</div>

<div class="content row">
  <?php
    if(isset($q)){
      ?>
  <div class="large-12 columns">
    <h4>Filtrando por "<?=$q;?>"</h4>
  </div>
      <?php
    } ?>
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Total</th>
          <th>Cliente</th>
          <th>Observaciones</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($ventas);$i++) { ?>
        <tr>
          <td><?=$ventas[$i]->id ?></td>
          <td><?=$ventas[$i]->fecha_carga ?></td>
          <td>$ <?=$ventas[$i]->total ?></td>
          <td><a href="<?=base_url('venta/listado') . '?q=' . $ventas[$i]->cuenta_corriente;?>"><?=nombre_cliente($ventas[$i]->cuenta_corriente) ?></a></td>
          <td><?=$ventas[$i]->observaciones ?></td>
          <td>
            <a href="<?=base_url('venta/ver_detalle');?>/<?=$ventas[$i]->id ?>"><i class="fa fa-eye"></i></a>
            <a href="<?=base_url('venta/eliminar');?>/<?=$ventas[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta venta?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>
