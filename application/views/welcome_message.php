		<div class="content row">
      <div class="large-6 columns panel">
        <a href="<?=base_url('venta/ingresar');?>" class="button radius nomargin expanded secondary">Ingresar nueva venta</a>
      </div>
      <div class="large-6 columns panel">
        <a href="<?=base_url('pago/ingresar');?>" class="button radius nomargin expanded success">Ingresar nuevo pago</a>
      </div>
      <div class="large-6 columns panel">
        <a href="<?=base_url('caja/nuevo_gasto');?>" class="button radius nomargin expanded alert">Ingresar nuevo gasto</a>
      </div>
      <div class="large-6 columns panel">
        <a href="<?=base_url('producto/nuevo');?>" class="button radius nomargin expanded warning">Ingresar nuevo producto</a>
      </div>
      <div class="large-6 columns panel">
        <a href="<?=base_url('cliente/nuevo');?>" class="button radius nomargin expanded hollow">Ingresar nuevo cliente</a>
      </div>
      <div class="large-6 columns panel">
        <a href="<?=base_url('proveedor/nuevo');?>" class="button radius nomargin expanded secondary hollow">Ingresar nuevo proveedor</a>
      </div>
    </div>

    <style>
      a {
        width: 100%;
      }
    </style>
