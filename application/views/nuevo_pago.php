<div class=" content row">
  <div class="large-12 columns">
    <h3>Cargar nuevo pago</h3>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <form action="<?=($editar == "editar")?base_url('pago/editar/'.$pago[0]->id):base_url('pago/ingresar');?>" method="POST" id="theform">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <div class="large-6 columns">
          <label>Cliente
            <?=drop_clientes($clientes,'-','','id_cliente');?>
          </label>
        </div>
        <div class="large-6 columns">
          <label>Fecha
            <input type="text" class="span2" name="fecha" value="<?=date('Y-m-d') ?>" id="reporte-gastos-dp1">
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Monto
            <input type="text" name="monto" value='<?=($editar == "editar")?$pago[0]->monto:""?>' />
          </label>
        </div>

      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Comentario
            <input type="text" name="comentario" value='<?=($editar == "editar")?$pago[0]->comentario:""?>' />
          </label>
        </div>
      </div>

      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Guardar' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>
