<div class="content row">
  <form action="#" method="GET">
    <div class="large-4 columns">
      <h3>Caja diaria</h3>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('caja/cerrar_diaria');?>" class="button postfix">Cerrar caja diaria</a>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('caja/ajuste');?>" class="button postfix">Cargar ajuste</a>
    </div>
  </form>
</div>

<div class="content row">
  <div class="large-12 columns">
    <h4 class='saldo'>Saldo actual: $ <?=$saldoactual[0]->saldo?></h4>
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Tipo</th>
          <th>Descripcion</th>
          <th>Monto</th>
          <th>Saldo</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($cajas);$i++) {
        $style='';
        $pesos = '$ ';
        if ($cajas[$i]->tipo == 'salida') {
          $style = 'style="background-color:#EAa7a7;"';
          $pesos = '$ -';
        }elseif ($cajas[$i]->tipo == 'entrada') {
          $style = 'style="background-color:#A7EAA7;"';
        }
        ?>
        <tr <?=$style?>>
          <td><?=$cajas[$i]->id ?></td>
          <td><?=$cajas[$i]->fecha ?></td>
          <td><?=$cajas[$i]->tipo ?></td>
          <td><?=$cajas[$i]->descripcion ?></td>
          <td><?=$pesos.$cajas[$i]->monto ?></td>
          <td>$<?=$cajas[$i]->saldo ?></td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
  <div class="large-12 columns">
    <h4>Ventas Tarjeta Debito: $ <?=$debito[0]->val?></h4>
    <h4>Ventas Tarjeta Credito: $ <?=$credito[0]->val?></h4>
    <h4>Ventas Cheque: $ <?=$cheque[0]->val?></h4>
  </div>
</div>

<style>
  .saldo {
    float: right;
    background-color: #1F6D1F;
    padding: 5px;
    width: 100%;
    text-align: center;
    color: #fff;
  }
</style>
