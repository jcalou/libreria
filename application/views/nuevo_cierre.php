<div class=" content row">
  <div class="large-12 columns">
    <h3>Cerrar caja diaria dia <?=date('d/m/Y')?></h3>
  </div>
</div>
<?php
$ventas = $pagoscli = $compras = $pagospro = $gastos = $ajustes = $debito_ = $credito_ = $cheque_ = 0;
for($i=0;$i<count($cajadiaria);$i++) {
  switch ($cajadiaria[$i]->tipo) {
    case "entrada":
      if (substr($cajadiaria[$i]->descripcion,0,3) == "Pag") {
        $pagoscli = $pagoscli + $cajadiaria[$i]->monto;
      }else if (substr($cajadiaria[$i]->descripcion,0,3) == "Ven") {
        $ventas = $ventas + $cajadiaria[$i]->monto;
      }
      break;
    case "salida":
      if (substr($cajadiaria[$i]->descripcion,0,3) == "Pag") {
        $pagospro = $pagospro + $cajadiaria[$i]->monto;
      }else if (substr($cajadiaria[$i]->descripcion,0,3) == "Com") {
        $compras = $compras + $cajadiaria[$i]->monto;
      }else if (substr($cajadiaria[$i]->descripcion,0,3) == "Gas") {
        $gastos = $gastos + $cajadiaria[$i]->monto;
      }
      break;
    case "ajuste":
      $ajustes = $ajustes + $cajadiaria[$i]->monto;
      break;
    default:
      //
  }
  if ($debito[0]->val != '') $debito_ = $debito[0]->val;
  if ($credito[0]->val != '') $credito_ = $credito[0]->val;
  if ($cheque[0]->val != '') $cheque_ = $cheque[0]->val;
}

?>
<div class=" content row">
  <div class="large-12 columns">
    <form action="<?=base_url('caja/cerrar_diaria');?>" method="POST" id="theform">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <fieldset name="Ingresos">
        <legend>Ingresos</legend>
        <div class="large-6 columns">
          <label>Ventas
            <input type="text" name="ventas" value='<?=$ventas?>' />
          </label>
        </div>
        <div class="large-6 columns">
          <label>Pagos de clientes
            <input type="text" name="pagoscli" value='<?=$pagoscli?>' />
          </label>
        </div>
        </fieldset>
      </div>
      <div class="row">
        <fieldset name="Egresos">
        <legend>Egresos</legend>
        <div class="large-4 columns">
          <label>Compra
            <input type="text" name="compras" value='<?=$compras?>' />
          </label>
        </div>
        <div class="large-4 columns">
          <label>Pago a proveedores
            <input type="text" name="pagospro" value='<?=$pagospro?>' />
          </label>
        </div>
        <div class="large-4 columns">
          <label>Gastos
            <input type="text" name="gastos" value='<?=$gastos?>' />
          </label>
        </div>
        </fieldset>
      </div>
      <div class="row">
        <fieldset name="Otros">
        <legend>Otros Movimientos</legend>
        <div class="large-4 columns">
          <label>Ajustes
            <input type="text" name="ajustes" value='<?=$ajustes?>' />
          </label>
        </div>
        </fieldset>
      </div>
      <div class="row">
        <fieldset name="Otros">
        <legend>Otros Medios de Pago</legend>
        <div class="large-4 columns">
          <label>Tarjeta Debito
            <input type="text" name="debito" value='<?=$debito_?>' />
          </label>
        </div>
        <div class="large-4 columns">
          <label>Tarjeta Credito
            <input type="text" name="credito" value='<?=$credito_?>' />
          </label>
        </div>
        <div class="large-4 columns">
          <label>Cheque
            <input type="text" name="cheque" value='<?=$cheque_?>' />
          </label>
        </div>
        </fieldset>
      </div>

      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Cargar cierre' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>
