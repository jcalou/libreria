<div class="content row">
  <form action="#" method="GET">
    <div class="large-8 columns">
      <h3>Cuenta corriente proveedor <?=nombre_proveedor($id)?></h3>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('compra/ingresar');?>" class="button postfix">Ingresar nueva compra</a>
    </div>
  </form>
</div>

<div class="content row">
  <div class="large-12 columns">
    <?php $totalcompras = 0; ?>
    <h4>Compras</h4>
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Total</th>
          <th>Observaciones</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($compras);$i++) { ?>
        <tr>
          <td><?=$compras[$i]->id ?></td>
          <td><?=$compras[$i]->fecha_carga ?></td>
          <td>$ <?=$compras[$i]->total ?><?php $totalcompras = $totalcompras + $compras[$i]->total; ?></td>
          <td><?=$compras[$i]->observaciones ?></td>
          <td>
            <a href="<?=base_url('compra/ver_detalle');?>/<?=$compras[$i]->id ?>"><i class="fa fa-eye"></i></a>
            <a href="<?=base_url('compra/eliminar');?>/<?=$compras[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta compra?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
    <h4>Total Compras: $ <?=$totalcompras?></h4>
  </div>
</div>

<div class="content row">
  <div class="large-12 columns">
    <?php $totalpagosproveedores = 0; ?>
    <h4>Pagos</h4>
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Monto</th>
          <th>Comentario</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($pagosproveedores);$i++) { ?>
        <tr>
          <td><?=$pagosproveedores[$i]->id ?></td>
          <td><?=$pagosproveedores[$i]->fecha ?></td>
          <td>$ <?=$pagosproveedores[$i]->monto ?><?php $totalpagosproveedores = $totalpagosproveedores + $pagosproveedores[$i]->monto; ?></td>
          <td><?=$pagosproveedores[$i]->comentario ?></td>
          <td>
            <a href="<?=base_url('pago/eliminar');?>/<?=$pagosproveedores[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta pago?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
    <h4>Total Pagos: $ <?=$totalpagosproveedores?></h4>
  </div>
</div>

<div class="content row">
  <div class="large-12 columns">
    <h3>Diferencia: $ <?=$totalcompras - $totalpagosproveedores?></h3>
  </div>
</div>
