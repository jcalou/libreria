<div class="content row">
  <form action="#" method="GET">
    <div class="large-4 columns">
      <h3>Listado de Clientes</h3>
    </div>
    <div class="large-4 columns">
      <div class="row collapse">
        <div class="large-10 columns">
          <input type="text" placeholder="Nombre del cliente" name="q" id="q" required />
        </div>
        <div class="large-2 columns">
          <input type="submit" value="Buscar" class="button postfix" />
        </div>
      </div>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('cliente/nuevo');?>" class="button postfix">Cargar nuevo Cliente</a>
    </div>
  </form>
</div>

<div class="content row">
  <?php
    if(isset($q)){
      ?>
  <div class="large-12 columns">
    <h4>Filtrando por "<?=$q;?>"</h4>
  </div>
      <?php
    } ?>
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Direccion</th>
          <th>Telefono</th>
          <th>Email</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($clientes);$i++) { ?>
        <tr>
          <td><?=$clientes[$i]->id ?></td>
          <td><?=$clientes[$i]->nombre ?></td>
          <td><?=$clientes[$i]->direccion ?></td>
          <td><?=$clientes[$i]->telefono ?></td>
          <td><?=$clientes[$i]->email ?></td>
          <td>
            <a href="<?=base_url('cliente/editar');?>/<?=$clientes[$i]->id ?>"><i class="fa fa-pencil"></i></a>
            <a href="<?=base_url('venta/cuenta_cliente');?>/<?=$clientes[$i]->id ?>"><i class="fa fa-money"></i></a>
            <a href="<?=base_url('cliente/eliminar');?>/<?=$clientes[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este cliente?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>
