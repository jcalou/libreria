    <div class="content row">
      <div class="large-12 columns">
        <h1>Detalle de venta id:<?=$id?> - Fecha <?=date('d/m/Y', strtotime($venta[0]->fecha_carga))?></h1>
      </div>
    </div>

    <div class="content row">
        <div class="large-8 columns">
          <table width="100%" id="items">
            <thead>
              <tr>
                <th>Producto</th>
                <th width="55">$ x U</th>
                <th width="55">Cant.</th>
                <th width="55">$</th>
              </tr>
              <?php for($i=0;$i<count($detalle);$i++) { ?>
              <tr>
                <td><?=$detalle[$i]->nombre_producto?></td>
                <td width="55"><?=$detalle[$i]->precio_unitario?></td>
                <td width="55"><?=$detalle[$i]->cantidad?></td>
                <td width="55"><?=$detalle[$i]->precio_unitario * $detalle[$i]->cantidad?></td>
              </tr>
              <?php } ?>
            </thead>
            <tbody>
            </tbody>
          </table>
          <h3>Forma de pago: <?=$venta[0]->efectivo?>
          <?php if($venta[0]->efectivo == 'cuentacorriente') {?>
            (<?=$venta[0]->nombre_cliente?>)
          <?php }?>
          </h3>
          <h3>Observaciones / Comentarios
            <?=$venta[0]->observaciones?>
          </h3>
        </div>
        <div class="large-4 columns">
          <fieldset class="total">
            <h3>TOTAL</h3>
            <h1 id="total">$ <?=$venta[0]->total?></h1>
            <h3>Pago con</h3>
            <h1 id="total">$ <?=$venta[0]->pago_con?></h1>
            <h3>Vuelto</h3>
            <h1 id="total">$ <?=$venta[0]->vuelto?></h1>
          </fieldset>
        </div>
    </div>
