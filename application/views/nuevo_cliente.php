<div class=" content row">
  <div class="large-12 columns">
    <h3><?=($editar == "editar")?"Editar cliente":"Cargar nuevo cliente"?></h3>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <form action="<?=($editar == "editar")?base_url('cliente/editar/'.$cliente[0]->id):base_url('cliente/nuevo');?>" method="POST" id="theform">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <div class="large-6 columns">
          <label>Nombre
            <input type="text" name="nombre" value='<?=($editar == "editar")?$cliente[0]->nombre:""?>' />
          </label>
        </div>
        <div class="large-6 columns">
          <label>Direccion
            <input type="text" name="direccion" value='<?=($editar == "editar")?$cliente[0]->direccion:""?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Tel&eacute;fono
            <input type="text" name="telefono" value='<?=($editar == "editar")?$cliente[0]->telefono:""?>' />
          </label>
        </div>
        <div class="large-6 columns">
          <label>Contacto
            <input type="text" name="contacto" value='<?=($editar == "editar")?$cliente[0]->contacto:""?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Email
            <input type="text" name="email" value='<?=($editar == "editar")?$cliente[0]->email:""?>' />
          </label>
        </div>
        <div class="large-6 columns">
          <label>CUIT
            <input type="text" name="cuit" value='<?=($editar == "editar")?$cliente[0]->cuit:""?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Observaciones
            <input type="text" name="observaciones" value='<?=($editar == "editar")?$cliente[0]->observaciones:""?>' />
          </label>
        </div>
      </div>

      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Guardar' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>
