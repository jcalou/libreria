<div class="content row">
  <form action="#" method="GET">
    <div class="large-4 columns">
      <h3>Listado de Compras</h3>
    </div>
    <div class="large-4 columns">
      &nbsp;
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('compra/ingresar');?>" class="button postfix">Ingresar nueva compra</a>
    </div>
  </form>
</div>

<div class="content row">
  <?php
    if(isset($q)){
      ?>
  <div class="large-12 columns">
    <h4>Filtrando por "<?=$q;?>"</h4>
  </div>
      <?php
    } ?>
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Total</th>
          <th>Cliente</th>
          <th>Observaciones</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($compras);$i++) { ?>
        <tr>
          <td><?=$compras[$i]->id ?></td>
          <td><?=$compras[$i]->fecha_carga ?></td>
          <td>$ <?=$compras[$i]->total ?></td>
          <td><a href="<?=base_url('compra/listado') . '?q=' . $compras[$i]->cuenta_corriente;?>"><?=nombre_cliente($compras[$i]->cuenta_corriente) ?></a></td>
          <td><?=$compras[$i]->observaciones ?></td>
          <td>
            <a href="<?=base_url('compra/ver_detalle');?>/<?=$compras[$i]->id ?>"><i class="fa fa-eye"></i></a>
            <a href="<?=base_url('compra/eliminar');?>/<?=$compras[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta compra?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>
