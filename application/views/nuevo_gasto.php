<?php echo '<script>var subrubro="-"</script>'; ?>
<div class=" content row">
  <div class="large-12 columns">
    <h3><?=($editar == "editar")?"Editar gasto":"Cargar nuevo gasto"?></h3>
  </div>
</div>
<form action="<?=($editar == "editar")?base_url('caja/editar_gasto/'.$gasto[0]->id):base_url('caja/nuevo_gasto');?>" method="POST" id="theform">
<div class="row">
  <div class="large-6 columns">
    <label>Fecha
      <input type="text" class="span2" name="fecha" value="<?=date('Y-m-d') ?>" id="reporte-gastos-dp1">
    </label>
  </div>
</div>
<div class="row">
  <div class="large-12 columns">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <div class="large-6 columns">
          <label>Rubro
            <?php echo drop_rubros($rubros,$rubro,'','rubro'); ?>
          </label>
        </div>
        <div class="large-6 columns">
          <label>Subrubro
            <select name="subrubro" id="subrubro">
              <option value="-">-</option>
            </select>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Descripcion
            <input type="text" name="descripcion" value='<?=($editar == "editar")?$gasto[0]->descripcion:rawurldecode($desc)?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Monto ($)
            <input type="text" name="monto" value='<?=($editar == "editar")?$gasto[0]->monto:rawurldecode($monto)?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Guardar' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>
