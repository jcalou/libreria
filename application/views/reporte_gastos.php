<?php echo '<script>var subrubro="'.$subrubro.'"</script>'; ?>
<div class="content row">
  <div class="large-12 columns">
    <h3>Reporte de Gastos</h3>
  </div>
</div>

<form method="POST" action="<?=base_url('caja/reporte_gastos'); ?>">
<div class="content row">
  <div class="large-2 columns">
    <label>Desde
      <input type="text" class="span2" name="desde" value="<?=$desde ?>" id="reporte-gastos-dp1">
    </label>
  </div>
  <div class="large-2 columns">
    <label>Hasta
      <input type="text" class="span2" name="hasta" value="<?=$hasta ?>" id="reporte-gastos-dp2">
    </label>
  </div>
  <div class="large-2 columns">
    <label>Rubro
      <?php echo drop_rubros($rubros,$rubro,'','rubro'); ?>
    </label>
  </div>
  <div class="large-2 columns">
    <label>Subrubro
      <select name="subrubro" id="subrubro">
        <option value="-">-</option>
      </select>
    </label>
  </div>
  <div class="large-2 columns">
    <input type="hidden" name="post" value="1" />
    <input type="submit" value='Buscar' class="button success" />
  </div>
</div>
</form>

<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th style="font-size:10px;">Fecha</th>
          <th>Rubro</th>
          <th>Sububro</th>
          <th style="width:330px;font-size:10px;">Descripcion</th>
          <th>Monto</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php
      $totaltotal = 0;
      $fecha = date("Y-m-d");$hora = date("H");
      for($i=0;$i<count($gastos);$i++) {
        $fecha2 = date("Y-m-d",strtotime($gastos[$i]->fecha));
        $hora2 = date("H",strtotime($gastos[$i]->fecha));
        ?>
        <tr>
          <td style="font-size:10px;"><?=$gastos[$i]->fecha ?></td>
          <td><?=$gastos[$i]->rubro ?></td>
          <td><?=$gastos[$i]->subrubro ?></td>
          <td style="width:330px;font-size:10px;"><?=$gastos[$i]->descripcion ?></td>
          <td>$<?=" ".$gastos[$i]->monto ?></td>
          <td>
            <?php
            if ($fecha == $fecha2){
             ?>
            <a href="<?=base_url('caja/eliminar_gasto');?>/<?=$gastos[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este gasto?')) { return false; }" target="_blank"><i class="fa fa-times"></i></a>
            <?php }
             ?>
          </td>
        </tr>
      <?php $totaltotal = $totaltotal + $gastos[$i]->monto; }; ?>
      </tbody>
    </table>
  </div>
  <div class="large-2 columns">
    <a href="<?=base_url('caja/reporte_imprimir').'/'.rawurlencode($desde).'/'.rawurlencode($hasta).'/'.rawurlencode($rubro).'/'.rawurlencode($subrubro);?>" class="button postfix" target="_blank">Imprimir Reporte</a>
  </div>
  <div class="large-3 columns">
    <h4>Total = $ <?=$totaltotal; ?></h4>
  </div>
</div>
