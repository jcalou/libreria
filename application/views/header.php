<!DOCTYPE html>
  <!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
  <!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
  <!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
  <!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo $description; ?>">

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/favicon.png') ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.css') ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/themes/base/jquery.ui.all.css') ?>" />
    <link rel='stylesheet' href="<?php echo base_url('assets/css/font-awesome.css') ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>" />
    <script src="<?php echo base_url('assets/js/vendor/modernizr.js') ?>"></script>
  </head>
  <body>

    <div class="topmenu">
      <div class="row">
        <nav class="top-bar" data-topbar>
          <ul class="title-area">
            <li class="name">
              <h1><a href="<?=base_url('');?>">Libreria</a></h1>
            </li>
          </ul>
          <section class="top-bar-section">
            <ul class="left">
              <li class="has-dropdown <?php if($page=="venta") { echo "active"; }; ?>">
                <a href="<?=base_url('venta/ingresar');?>">Venta</a>
                <ul class="dropdown">
                  <li><a href="<?=base_url('venta/ingresar');?>">Ingresar nueva</a></li>
                  <li><a href="<?=base_url('venta/listado');?>">Listado</a></li>
                </ul>
              </li>
              <li class="has-dropdown <?php if($page=="caja") { echo "active"; }; ?>">
                <a href="<?=base_url('caja/listado_gastos');?>">Gastos</a>
                <ul class="dropdown">
                  <li><a href="<?=base_url('caja/listado_gastos');?>">Listar Gastos</a></li>
                  <li><a href="<?=base_url('caja/nuevo_gasto');?>">Cargar Nuevo Gasto</a></li>
                  <li><a href="<?=base_url('caja/reporte_gastos');?>">Reporte de Gastos</a></li>
                  <li><a href="<?=base_url('caja/informe_gastos');?>">Informe de gastos Totales</a></li>
                </ul>
              </li>
              <li class="has-dropdown <?php if($page=="productos") { echo "active"; }; ?>">
                <a href="<?=base_url('producto/listado');?>">Productos</a>
                <ul class="dropdown">
                  <li><a href="<?=base_url('producto/listado');?>">Listar</a></li>
                  <li><a href="<?=base_url('producto/nuevo');?>">Agregar</a></li>
                  <li><a href="<?=base_url('producto/listado_stock_minimo');?>">Stock Minimo</a></li>
                  <li><a href="<?=base_url('producto/listado_stock_negativo');?>">Stock Negativo</a></li>
                  <li><a href="<?=base_url('producto/redondearprecios');?>">Redondear precios</a></li>
                </ul>
              </li>
              <li class="has-dropdown <?php if($page=="clientes") { echo "active"; }; ?>">
                <a href="<?=base_url('cliente/listado');?>">Clientes</a>
                <ul class="dropdown">
                  <li><a href="<?=base_url('cliente/listado');?>">Listado clientes</a></li>
                  <li><a href="<?=base_url('cliente/nuevo');?>">Agregar cliente</a></li>
                  <li><a href="<?=base_url('pago/listado');?>">Listado pagos a clientes</a></li>
                  <li><a href="<?=base_url('pago/ingresar');?>">Ingresar pago</a></li>
                </ul>
              </li>
              <li class="has-dropdown <?php if($page=="proveedores") { echo "active"; }; ?>">
                <a href="<?=base_url('proveedor/listado');?>">Proveedores</a>
                <ul class="dropdown">
                  <li><a href="<?=base_url('proveedor/listado');?>">Listar</a></li>
                  <li><a href="<?=base_url('proveedor/nuevo');?>">Agregar</a></li>
                  <li><a href="<?=base_url('pagoproveedores/listado');?>">Listado pagos a proveedores</a></li>
                  <li><a href="<?=base_url('pagoproveedores/ingresar');?>">Ingresar pago</a></li>
                </ul>
              </li>
              <li class="has-dropdown <?php if($page=="compra") { echo "active"; }; ?>">
                <a href="<?=base_url('compra/ingresar');?>">Compra</a>
                <ul class="dropdown">
                  <li><a href="<?=base_url('compra/ingresar');?>">Ingresar nueva</a></li>
                  <li><a href="<?=base_url('compra/listado');?>">Listado</a></li>
                </ul>
              </li>
              <li class="has-dropdown <?php if($page=="cajas") { echo "active"; }; ?>">
                <a href="<?=base_url('caja/listado');?>">$ CAJA DIARIA $</a>
                <ul class="dropdown">
                  <li><a href="<?=base_url('caja/reporte_caja');?>">Reporte Dias Anteriores</a></li>
                </ul>
              </li>

            </ul>
            <ul class="right">
              <li><p>Fecha: <?=date("d-m-Y");?></p></li>
              <!-- <li><a href="<?=base_url('caja/listado');?>" class="caja">Caja: $ 0</a></li> -->
            </ul>
          </section>
        </nav>
      </div>
    </div>
