<div class="content row">
  <form action="#" method="GET">
    <div class="large-4 columns">
      <h3>Listado de Proveedores</h3>
    </div>
    <div class="large-4 columns">
      <div class="row collapse">
        <div class="large-10 columns">
          <input type="text" placeholder="Nombre del proveedor" name="q" id="q" required />
        </div>
        <div class="large-2 columns">
          <input type="submit" value="Buscar" class="button postfix" />
        </div>
      </div>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('proveedor/nuevo');?>" class="button postfix">Cargar nuevo Proveedor</a>
    </div>
  </form>
</div>

<div class="content row">
  <?php
    if(isset($q)){
      ?>
  <div class="large-12 columns">
    <h4>Filtrando por "<?=$q;?>"</h4>
  </div>
      <?php
    } ?>
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Direccion</th>
          <th>Telefono</th>
          <th>Email</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($proveedores);$i++) { ?>
        <tr>
          <td><?=$proveedores[$i]->id ?></td>
          <td><?=$proveedores[$i]->nombre ?></td>
          <td><?=$proveedores[$i]->direccion ?></td>
          <td><?=$proveedores[$i]->telefono ?></td>
          <td><?=$proveedores[$i]->email ?></td>
          <td>
            <a href="<?=base_url('proveedor/editar');?>/<?=$proveedores[$i]->id ?>"><i class="fa fa-pencil"></i></a>
            <a href="<?=base_url('compra/cuenta_proveedor');?>/<?=$proveedores[$i]->id ?>"><i class="fa fa-money"></i></a>
            <a href="<?=base_url('proveedor/eliminar');?>/<?=$proveedores[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este proveedor?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>
