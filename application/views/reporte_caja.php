<div class="content row">
  <div class="large-12 columns">
    <h3>Reporte de Caja</h3>
  </div>
</div>

<form method="POST" action="<?=base_url('caja/reporte_caja'); ?>">
<div class="content row">
  <div class="large-2 columns">
    <label>Desde
      <input type="text" class="span2" name="desde" value="<?=$desde ?>" id="reporte-gastos-dp1">
    </label>
  </div>
  <div class="large-2 columns">
    <label>Hasta
      <input type="text" class="span2" name="hasta" value="<?=$hasta ?>" id="reporte-gastos-dp2">
    </label>
  </div>

  <div class="large-2 columns">
    <input type="hidden" name="post" value="1" />
    <input type="submit" value='Buscar' class="button success" />
  </div>
</div>
</form>

<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Tipo</th>
          <th>Descripcion</th>
          <th>Monto</th>
          <th>Saldo</th>
        </tr>
      </thead>
      <tbody>
      <?php
      $totaltotal = 0;
      $fecha2 = date("Y-m-d",strtotime($caja[0]->fecha));
      for($i=0;$i<count($caja);$i++) {
        $fecha = date("Y-m-d",strtotime($caja[$i]->fecha));
        if ($fecha != $fecha2) {
          ;
          echo "<tr style='background-color:#000;color:#fff'><td style='background-color:#000;color:#fff' colspan='6'>Total otros movimientos dia ".$fecha2." - Debitos: $" . debitos($fecha2) . " - Creditos: $" . creditos($fecha2) . " - Cheques: $" . cheques($fecha2) . "</td></tr>";
        }
        $style='';
        $pesos = '$ ';
        if ($caja[$i]->tipo == 'salida') {
          $style = 'style="background-color:#EAa7a7;"';
          $pesos = '$ -';
        }elseif ($caja[$i]->tipo == 'entrada') {
          $style = 'style="background-color:#A7EAA7;"';
        }
        $fecha2 = date("Y-m-d",strtotime($caja[$i]->fecha));
        $hora2 = date("H:m",strtotime($caja[$i]->fecha));
        ?>
        <tr <?=$style?>>
          <td><?=$caja[$i]->id ?></td>
          <td><?=$fecha2 . ' ' . $hora2 ?> hs.</td>
          <td><?=$caja[$i]->tipo ?></td>
          <td><?=$caja[$i]->descripcion ?></td>
          <td>$<?=" ".$caja[$i]->monto ?></td>
          <td>$<?=" ".$caja[$i]->saldo ?></td>
        </tr>
      <?php  }; ?>
      </tbody>
    </table>
  </div>
  <!-- <div class="large-2 columns">
    <a href="<?=base_url('caja/reporte_imprimir').'/'.rawurlencode($desde).'/'.rawurlencode($hasta).'/'.rawurlencode($rubro).'/'.rawurlencode($subrubro);?>" class="button postfix" target="_blank">Imprimir Reporte</a>
  </div>
  <div class="large-3 columns">
    <h4>Total = $ <?=$totaltotal; ?></h4>
  </div> -->
</div>
