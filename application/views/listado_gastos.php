<div class="content row">
  <form action="#" method="GET">
    <div class="large-9 columns">
      <h3>Listado de Gastos</h3>
    </div>
    <div class="large-3 columns">
      <a href="<?=base_url('caja/nuevo_gasto');?>" class="button postfix">Cargar Nuevo Gasto</a>
    </div>
  </form>
</div>

<div class="content row">
  <div class="large-12 columns">
    <?php echo $pags; ?>
  </div>
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th style="font-size:10px;">Fecha</th>
          <th>Rubro</th>
          <th>Subrubro</th>
          <th style="width:350px;font-size:10px;">Descripcion</th>
          <th>Monto</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $fecha = date("Y-m-d");$hora = date("H");
      for($i=0;$i<count($gastos);$i++) {
        $fecha2 = date("Y-m-d",strtotime($gastos[$i]->fecha));
        $hora2 = date("H",strtotime($gastos[$i]->fecha));
        ?>
        <tr>
          <td style="font-size:10px;"><?=$gastos[$i]->fecha ?></td>
          <td><?=nombre_rubro($gastos[$i]->rubro) ?></td>
          <td><?=nombre_subrubro($gastos[$i]->subrubro) ?></td>
          <td style="width:350px;font-size:10px;"><?=$gastos[$i]->descripcion ?></td>
          <td>$<?=" ".$gastos[$i]->monto ?></td>
          <td>
            <?php
            if ($fecha == $fecha2 || $this->session->userdata("user_nivel")==1){
              if (($hora < "13" && $hora2 < "13") || (($hora >= "13" && $hora2 >= "13")) || $this->session->userdata("user_nivel")==1) {
             ?>
            <a href="<?=base_url('caja/eliminar_gasto');?>/<?=$gastos[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este gasto?')) { return false; }"><i class="fa fa-times"></i></a>
            <?php } }
             ?>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
  <div class="large-12 columns">
    <?php echo $pags; ?>
  </div>
</div>
