    <footer></footer>
    <?php
    echo '<script>var ipsite="' . IPSITE . '"</script>';
    echo '<script>var ipsiteurl="' . IPSITEURL . '"</script>';
    ?>
    <script src="<?php echo base_url('assets/js/vendor/jquery.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/jquery.ui.core.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/jquery.ui.widget.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/jquery.ui.position.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/jquery.ui.menu.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/jquery.ui.autocomplete.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/foundation.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/venta.js') ?>"></script>

    <?php
    echo "<script>";
    echo "var availableTags = [";
    $firstime = true;
    for($i=0;$i<count($producto);$i++) {
      if ($firstime){
        echo '{ id:"' . $producto[$i]->id . '", label: "' . $producto[$i]->nombre . '", value: "' . $producto[$i]->nombre . '", precio: "' . $producto[$i]->precio . '", nombre: "' . $producto[$i]->nombre . '", bc: "' . $producto[$i]->codigo_de_barra . '" }';
        $firstime = false;
      }else{
          echo ',{ id:"' . $producto[$i]->id . '", label: "' . $producto[$i]->nombre . '", value: "' . $producto[$i]->nombre . '", precio: "' . $producto[$i]->precio . '", nombre: "' . $producto[$i]->nombre . '", bc: "' . $producto[$i]->codigo_de_barra . '" }';
      }
    }
    echo "];";
    echo "</script>";
    ?>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
