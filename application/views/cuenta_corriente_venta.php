<div class="content row">
  <form action="#" method="GET">
    <div class="large-8 columns">
      <h3>Cuenta corriente cliente <?=nombre_cliente($id)?></h3>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('venta/ingresar');?>" class="button postfix">Ingresar nueva venta</a>
    </div>
  </form>
</div>

<div class="content row">
  <div class="large-12 columns">
    <?php $totalventas = 0; ?>
    <h4>Ventas</h4>
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Total</th>
          <th>Observaciones</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($ventas);$i++) { ?>
        <tr>
          <td><?=$ventas[$i]->id ?></td>
          <td><?=$ventas[$i]->fecha_carga ?></td>
          <td>$ <?=$ventas[$i]->total ?><?php $totalventas = $totalventas + $ventas[$i]->total; ?></td>
          <td><?=$ventas[$i]->observaciones ?></td>
          <td>
            <a href="<?=base_url('venta/ver_detalle');?>/<?=$ventas[$i]->id ?>"><i class="fa fa-eye"></i></a>
            <a href="<?=base_url('venta/eliminar');?>/<?=$ventas[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta venta?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
    <h4>Total Ventas: $ <?=$totalventas?></h4>
  </div>
</div>

<div class="content row">
  <div class="large-12 columns">
    <?php $totalpagos = 0; ?>
    <h4>Pagos</h4>
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Monto</th>
          <th>Comentario</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($pagos);$i++) { ?>
        <tr>
          <td><?=$pagos[$i]->id ?></td>
          <td><?=$pagos[$i]->fecha ?></td>
          <td>$ <?=$pagos[$i]->monto ?><?php $totalpagos = $totalpagos + $pagos[$i]->monto; ?></td>
          <td><?=$pagos[$i]->comentario ?></td>
          <td>
            <a href="<?=base_url('pago/eliminar');?>/<?=$pagos[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta pago?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
    <h4>Total Pagos: $ <?=$totalpagos?></h4>
  </div>
</div>

<div class="content row">
  <div class="large-12 columns">
    <h3>Diferencia: $ <?=$totalventas - $totalpagos?></h3>
  </div>
</div>
