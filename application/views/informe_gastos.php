<div class="content row">
  <div class="large-12 columns">
    <h3>Informe de Gastos por rango de fechas</h3>
  </div>
</div>

<form method="POST" action="<?=base_url('caja/informe_gastos'); ?>" target="_blank">
<div class="content row">
  <div class="large-3 columns">
    <label>Desde
      <input type="text" class="span2" name="desde" value="<?=date('Y-m-d'); ?>" id="reporte-gastos-dp1">
    </label>
  </div>
  <div class="large-3 columns">
    <label>Hasta
      <input type="text" class="span2" name="hasta" value="<?=date('Y-m-d'); ?>" id="reporte-gastos-dp2">
    </label>
  </div>
  <div class="large-3 columns">
    <label style="margin: 26px 0 0 0;font-size: 16px;">Totales rubro/ subrubro
      <input type="checkbox" name="totales" />
    </label>
  </div>
  <div class="large-3 columns">
    <input type="hidden" name="post" value="1" />
    <input type="submit" value='Obtener Informe' class="button success" style="margin: 10px 0 0 0;" />
  </div>
</div>
</form>
