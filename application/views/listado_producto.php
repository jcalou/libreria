<div class="content row">
  <form action="#" method="GET">
    <div class="large-4 columns">
      <h3>Listado de Productos</h3>
    </div>
    <div class="large-4 columns">
      <div class="row collapse">
        <div class="large-10 columns">
          <input type="text" placeholder="Nombre del producto" name="q" id="q" required />
        </div>
        <div class="large-2 columns">
          <input type="submit" value="Buscar" class="button postfix" />
        </div>
      </div>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('producto/nuevo');?>" class="button postfix">Cargar nuevo Producto</a>
    </div>
  </form>
</div>

<div class="content row">
  <?php
    if(isset($q)){
      ?>
  <div class="large-12 columns">
    <h4>Filtrando por "<?=$q;?>"</h4>
  </div>
      <?php
    }else{ ?>
  <div class="large-12 columns">
    <?php echo $pags; ?>
  </div>
  <?php
    } ?>
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Marca</th>
          <th>Rubro</th>
          <th>Subrubro</th>
          <th>Proveedor</th>
          <th>C&oacute;digo de Barra</th>
          <th>Stock</th>
          <th>Stock<br/>M&iacute;nimo</th>
          <th>Precio</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($productos);$i++) { ?>
        <tr>
          <td><?=$productos[$i]->id ?></td>
          <td><?=$productos[$i]->nombre ?></td>
          <td><?=$productos[$i]->marca ?></td>
          <td><?=nombre_tipo($productos[$i]->tipo) ?></td>
          <td><?=nombre_subtipo($productos[$i]->subtipo) ?></td>
          <td><a href="<?=base_url('proveedor/editar');?>/<?=$productos[$i]->e_id ?>"><?=$productos[$i]->e_nombre ?></a></td>
          <td><?=$productos[$i]->codigo_de_barra ?></td>
          <td class="<?=($productos[$i]->stock > $productos[$i]->stock_minimo) && (($productos[$i]->stock - $productos[$i]->stock_minimo) <= STOCK_DIF)?" warning":""?><?=($productos[$i]->stock <= $productos[$i]->stock_minimo) && ($productos[$i]->stock > 0)?" alert":""?><?=($productos[$i]->stock <= 0)?" emergency":""?>"><?=$productos[$i]->stock ?></td>
          <td><?=$productos[$i]->stock_minimo ?></td>
          <td><?=$productos[$i]->precio ?></td>
          <td>
            <a href="<?=base_url('producto/editar');?>/<?=$productos[$i]->id ?>"><i class="fa fa-pencil"></i></a>
            <a href="<?=base_url('producto/eliminar');?>/<?=$productos[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este producto?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
  <?php
    if(isset($q)){
    }else{ ?>
  <div class="large-12 columns">
    <?php echo $pags; ?>
  </div>
  <?php
    } ?>
</div>
