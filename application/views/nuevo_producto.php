    <?php echo '<script>var subrubro="-"</script>'; ?>
    <div class=" content row">
      <div class="large-12 columns">
        <h3><?=($editar == "editar")?"Editar producto":"Cargar nuevo producto"?></h3>
      </div>
    </div>
    <div class=" content row">
      <div class="large-12 columns">
        <form action="<?=($editar == "editar")?base_url('producto/editar/'.$producto[0]->id):base_url('producto/nuevo');?>" method="POST" id="theform">
          <?php  if($editar == "editar") { ?>
            <input type="hidden" name="id" value='<?=$producto[0]->id; ?>'>
          <?php }else{ ?>
            <input type="hidden" name="id" value="0">
          <?php } ?>
          <div class="row">
            <div class="large-6 columns">
              <label>Nombre
                <input type="text" name="nombre" value='<?=($editar == "editar")?$producto[0]->nombre:""?>' required />
              </label>
            </div>
            <div class="large-3 columns">
              <label>Rubro
                <?php echo drop_rubros($rubros,$rubro,'','tipo'); ?>
              </label>
            </div>
            <div class="large-3 columns">
              <label>Subrubro
                <?php if($editar == "editar") {
                   echo '<script>var subrubro='.$producto[0]->subtipo.'</script>';
                  }?>
                <select name='subtipo' id='subtipo'><option value=''>-</option></select>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-6 columns">
              <label>Marca
                <input type="text" name="marca" value='<?=($editar == "editar")?$producto[0]->marca:""?>' />
              </label>
            </div>
            <div class="large-6 columns">
              <label>Codigo de Barra
                <input type="text" name="codigo_de_barra" id="codigo_de_barra" value='<?=($editar == "editar")?$producto[0]->codigo_de_barra:""?>' />
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-9 columns">
              <label>Proveedor
                <?=drop_proveedores($proveedores,$id_proveedor,'','proveedor');?>
              </label>
            </div>
            <div class="large-3 columns">
              <label>¿No existe?
                <a href="<?=base_url('proveedor/nuevo');?>" class="button agregarproveedor">Agregar Proveedor</a>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <label>Descripcion
                <textarea name="descripcion" id="descripcion"><?=($editar == "editar")?$producto[0]->descripcion:""?></textarea>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-6 columns">
              <label>Stock
                <input type="number" name="stock" value='<?=($editar == "editar")?$producto[0]->stock:"1"?>' required />
              </label>
            </div>
            <div class="large-6 columns">
              <label>Stock minimo
                <input type="number" name="stock_minimo" value='<?=($editar == "editar")?$producto[0]->stock_minimo:"1"?>' required />
              </label>
            </div>
          </div>
          <div class="row collapse">
            <div class="small-3 large-1 columns">
              <label>Costo
                <span class="prefix">$</span>
              </label>
            </div>
            <div class="small-9 large-2 columns">
              <label>&nbsp;
                <input type="text" name="costo" value='<?=($editar == "editar")?$producto[0]->costo:""?>' id="costo" />
              </label>
            </div>
            <div class="large-1 columns">
              <label>&nbsp;
              </label>
            </div>
            <div class="small-3 large-1 columns">
              <label>Margen
                <span class="prefix">%</span>
              </label>
            </div>
            <div class="small-9 large-2 columns">
              <label>&nbsp;
                <input type="text" name="margen" value='<?=($editar == "editar")?$producto[0]->margen:""?>' id="margen" />
              </label>
            </div>
            <div class="large-1 columns">
              <label>&nbsp;
              </label>
            </div>
            <div class="small-3 large-1 columns">
              <label>Precio
                <span class="prefix">$</span>
              </label>
            </div>
            <div class="small-9 large-2 columns">
              <label>&nbsp;
                <input type="text" name="precio" value='<?=($editar == "editar")?$producto[0]->precio:""?>' required id="precio" />
              </label>
            </div>
            <div class="large-1 columns">
              <label>&nbsp;
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-4 columns">
              <input type="hidden" name="post" value="1" />
              <input type="submit" value='Guardar' class="button success" />
              <a href="javascript:history.back();" class="button secondary">Cancelar</a>
            </div>
          </div>

        </form>
      </div>
    </div>
