<div class="content row">
  <form action="#" method="GET">
    <div class="large-8 columns">
      <h3>Listado de Pagos a proveedores</h3>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('pagoproveedores/ingresar');?>" class="button postfix">Ingresar nuevo pago</a>
    </div>
  </form>
</div>

<div class="content row">
  <?php
    if(isset($q)){
      ?>
  <div class="large-12 columns">
    <h4>Filtrando por "<?=$q;?>"</h4>
  </div>
      <?php
    } ?>
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Total</th>
          <th>Proveedor</th>
          <th>Comentario</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($pagos);$i++) { ?>
        <tr>
          <td><?=$pagos[$i]->id ?></td>
          <td><?=$pagos[$i]->fecha ?></td>
          <td>$ <?=$pagos[$i]->monto ?></td>
          <td><a href="<?=base_url('pagoproveedores/listado') . '?q=' . $pagos[$i]->id_proveedor;?>"><?=nombre_proveedor($pagos[$i]->id_proveedor) ?></a></td>
          <td><?=$pagos[$i]->comentario ?></td>
          <td>
            <a href="<?=base_url('pagoproveedores/eliminar');?>/<?=$pagos[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta pago?')) { return false; }"><i class="fa fa-times"></i></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>
