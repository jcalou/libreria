<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['caja/(:any)'] = 'caja/$1';
$route['venta/(:any)'] = 'venta/$1';
$route['compra/(:any)'] = 'compra/$1';
$route['pago/(:any)'] = 'pago/$1';
$route['producto/(:any)'] = 'producto/$1';
$route['proveedor/(:any)'] = 'proveedor/$1';
$route['pago/(:any)'] = 'pago/$1';
$route['pagoproveedores/(:any)'] = 'pagoproveedores/$1';
$route['cliente/(:any)'] = 'cliente/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
