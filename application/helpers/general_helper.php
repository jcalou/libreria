<?php

function subrubros($tipo, $selected='') {
  $query = "SELECT * from subtipo where id_tipo=" . $tipo;
  $ret = mysql_query($query);

  echo "<option value='-'>Seleccione subrubro</option>";
  while($row = mysql_fetch_assoc($ret)) {
    if ($row['id']==$selected){
        echo "<option value='".$row['id']."' selected='selected'>".$row['nombre']."</option>";
    }else{
        echo "<option value='".$row['id']."'>".$row['nombre']."</option>";
    }
  }
}

function drop_rubros($rubros,$id="",$req,$id_tag="rubros") {
  $required="";
  if($req=="required"){$required='class="required"';}
  $select = '<select name="rubro" id="'.$id_tag.'" '.$required.'><option value="-">Seleccione rubro</option>';
  for ($row = 0; $row < count($rubros); $row++){
    if ($id==$rubros[$row]->id){
      $select = $select . '<option selected="selected" value="'.$rubros[$row]->id.'">'.$rubros[$row]->nombre.'</option>';
    }else{
      $select = $select . '<option value="'.$rubros[$row]->id.'">'.$rubros[$row]->nombre.'</option>';
    }
  }
  $select = $select . '</select>';
  return $select;
}

function drop_clientes($clientes,$id="",$req,$id_tag="clientes") {
  $required="";
  if($req=="required"){$required='class="required"';}
  $select = '<select name="clientes" id="'.$id_tag.'" '.$required.'><option value="-">Seleccione cliente</option>';
  for ($row = 0; $row < count($clientes); $row++){
    if ($id==$clientes[$row]->id){
      $select = $select . '<option selected="selected" value="'.$clientes[$row]->id.'">'.$clientes[$row]->nombre.'</option>';
    }else{
      $select = $select . '<option value="'.$clientes[$row]->id.'">'.$clientes[$row]->nombre.'</option>';
    }
  }
  $select = $select . '</select>';
  return $select;
}

function drop_proveedores($proveedores,$id="",$req,$id_tag="proveedores") {
  $required="";
  if($req=="required"){$required='class="required"';}
  $select = '<select name="proveedores" id="'.$id_tag.'" '.$required.'><option value="-">Seleccione proveedor</option>';
  for ($row = 0; $row < count($proveedores); $row++){
    if ($id==$proveedores[$row]->id){
      $select = $select . '<option selected="selected" value="'.$proveedores[$row]->id.'">'.$proveedores[$row]->nombre.'</option>';
    }else{
      $select = $select . '<option value="'.$proveedores[$row]->id.'">'.$proveedores[$row]->nombre.'</option>';
    }
  }
  $select = $select . '</select>';
  return $select;
}

function drop_meses($id="0",$required="required") {
  $select = '<select name="mes" id="mes" '.$required.' >';
  $select .= '<option value="">Elija una mes</option>';
  if ($id=="1"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="1">Enero</option>';
  if ($id=="2"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="2">Febrero</option>';
  if ($id=="3"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="3">Marzo</option>';
  if ($id=="4"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="4">Abril</option>';
  if ($id=="5"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="5">Mayo</option>';
  if ($id=="6"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="6">Junio</option>';
  if ($id=="7"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="7">Julio</option>';
  if ($id=="8"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="8">Agosto</option>';
  if ($id=="9"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="9">Septiembre</option>';
  if ($id=="10"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="10">Octubre</option>';
  if ($id=="11"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="11">Noviembre</option>';
  if ($id=="12"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="12">Diciembre</option>';
  $select .= '</select>';

  return $select;
}

function drop_anios($id="0",$required="required") {
  $select = '<select name="anio" id="anio" '.$required.' >';
  $select .= '<option value="">Elija una año</option>';
  if ($id=="2015"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="2015">2015</option>';
  if ($id=="2016"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="2016">2016</option>';
  if ($id=="2017"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="2017">2017</option>';
  if ($id=="2018"){$sel=" selected='selected'";}else{$sel="";}
  $select .= '<option '.$sel.' value="2018">2018</option>';
  $select .= '</select>';

  return $select;
}

function find ($string, $array = array ())
{
  foreach ($array as $key => $value) {
    unset ($array[$key]);
    if (strpos($value, $string) !== false ) {
      $array[$key] = $value;
    }
  }
  return $array;
}

function find_strict ($string, $array = array ())
{
  foreach ($array as $key => $value) {
    unset ($array[$key]);
    if ($value == $string) {
      $array[$key] = $value;
    }
  }
  return $array;
}

function nombre_tipo($id = 0) {
  if ($id == 0) return "";
  $CI = get_instance();
  $CI->load->model('producto_model');
  $tipo = $CI->producto_model->getTipoName($id);
  return $tipo[0]->nombre;
}

function nombre_subtipo($id = 0) {
  if ($id == 0) return "";
  $CI = get_instance();
  $CI->load->model('producto_model');
  $tipo = $CI->producto_model->getSubTipoName($id);
  return $tipo[0]->nombre;
}

function nombre_rubro($id = 0) {
  if ($id == 0) return "";
  $CI = get_instance();
  $CI->load->model('caja_model');
  $tipo = $CI->caja_model->getRubroName($id);
  return $tipo[0]->nombrelargo;
}

function nombre_subrubro($id = 0) {
  if ($id == 0) return "";
  $CI = get_instance();
  $CI->load->model('caja_model');
  $tipo = $CI->caja_model->getSubRubroName($id);
  return $tipo[0]->nombrelargo;
}

function nombre_cliente($id = 0) {
  $CI = get_instance();
  $CI->load->model('cliente_model');
  if ($id == 0 || $id == '' || $id == '-') {return '';}
  $tipo = $CI->cliente_model->getName($id);
  return $tipo[0]->nombre;
}

function nombre_proveedor($id = 0) {
  $CI = get_instance();
  $CI->load->model('proveedor_model');
  if ($id == 0 || $id == '' || $id == '-') {return '';}
  $tipo = $CI->proveedor_model->getName($id);
  return $tipo[0]->nombre;
}

function debitos($dia) {
  if ($dia == '' || $dia == '-') {return '';}
  $CI = get_instance();
  $CI->load->model('caja_model');
  $monto = $CI->caja_model->get_caja_diaria_debito_dia($dia);
  return $monto[0]->val;
}

function creditos($dia) {
  if ($dia == '' || $dia == '-') {return '';}
  $CI = get_instance();
  $CI->load->model('caja_model');
  $monto = $CI->caja_model->get_caja_diaria_credito_dia($dia);
  return $monto[0]->val;
}

function cheques($dia) {
  if ($dia == '' || $dia == '-') {return '';}
  $CI = get_instance();
  $CI->load->model('caja_model');
  $monto = $CI->caja_model->get_caja_diaria_cheque_dia($dia);
  return $monto[0]->val;
}
