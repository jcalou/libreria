<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pago extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('pago_model');
    $this->load->model('caja_model');
    $this->load->model('cliente_model');
  }

  public function listado() {
    $data['title'] = 'Listado de Pagos - Libreria';
    $data['description'] = 'Listado de Pagos - Libreria';
    $data['page'] = 'pago';

    if($this->input->get('q')) {
      $data['pagos'] = $this->pago_model->buscar_pagos($this->input->get('q'));
      $data['q'] = $this->input->get('q');

      $this->load->view('header', $data);
      $this->load->view('listado_pago', $data);
      $this->load->view('footer');
    }else{
      $data['pagos'] = $this->pago_model->get_pagos();

      $this->load->view('header', $data);
      $this->load->view('listado_pago', $data);
      $this->load->view('footer');
    }
  }

  public function ingresar(){

    $data['title'] = 'Cargar nueva pago - Libreria';
    $data['description'] = 'Cargar nueva pago - Libreria';
    $data['page'] = 'pago';
    $data['editar'] = 'nuevo';

    if($this->input->post('post') && $this->input->post('post')=="1"){
        //agregar
        $this->pago_model->id_cliente = $this->input->post('clientes');
        $this->pago_model->fecha = $this->input->post('fecha'). ' ' . date('H:i:s');
        $this->pago_model->monto = $this->input->post('monto');
        $this->pago_model->comentario = $this->input->post('comentario');

        $result = $this->pago_model->agregar_pago();

        $saldoactual = $this->caja_model->get_saldo();

        $this->caja_model->fecha = date("Y-m-d H:i:s");
        $this->caja_model->tipo = 'entrada';
        $this->caja_model->descripcion = 'Pago de cliente id: ' . $result;
        $this->caja_model->monto = $this->input->post('monto');
        $this->caja_model->saldo = $saldoactual[0]->saldo + $this->input->post('monto');

        $this->caja_model->agregar_caja();

        redirect('pago/listado');
        exit;
    }else{

      $data['clientes'] = $this->cliente_model->get_clientes();

      $this->load->view('header', $data);
      $this->load->view('nuevo_pago', $data);
      $this->load->view('footer_gastos', $data);
    }
  }

}
