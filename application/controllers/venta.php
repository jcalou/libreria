<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('venta_model');
    $this->load->model('pago_model');
    $this->load->model('caja_model');
    $this->load->model('producto_model');
    $this->load->model('cliente_model');
  }

  public function listado() {
    $data['title'] = 'Listado de Ventas - Libreria';
    $data['description'] = 'Listado de Ventas - Libreria';
    $data['page'] = 'venta';

    if($this->input->get('q')) {
      $data['ventas'] = $this->venta_model->buscar_ventas($this->input->get('q'));
      $data['q'] = $this->input->get('q');

      $this->load->view('header', $data);
      $this->load->view('listado_venta', $data);
      $this->load->view('footer');
    }else{
      $data['ventas'] = $this->venta_model->get_ventas();

      $this->load->view('header', $data);
      $this->load->view('listado_venta', $data);
      $this->load->view('footer');
    }
  }

  public function ver_detalle($id) {
    $data['title'] = 'Detalle de Venta - Libreria';
    $data['description'] = 'Detalle de Venta - Libreria';
    $data['page'] = 'venta';

    $data['id'] = $id;
    $data['venta'] = $this->venta_model->getId($id);
    $data['detalle'] = $this->venta_model->getIdDetalle($id);

    $this->load->view('header', $data);
    $this->load->view('ver_detalle_venta', $data);
    $this->load->view('footer');

  }

  public function cuenta_cliente($id = 0) {
    $data['title'] = 'Cuenta corriente del cliente - Libreria';
    $data['description'] = 'Cuenta corriente del cliente - Libreria';
    $data['page'] = 'venta';

    $data['ventas'] = $this->venta_model->buscar_ventas_cc($id);
    $data['pagos'] = $this->pago_model->buscar_pagos($id);
    $data['id'] = $id;

    $this->load->view('header', $data);
    $this->load->view('cuenta_corriente_venta', $data);
    $this->load->view('footer');

  }

  public function ingresar(){
    $data['title'] = 'Cargar nueva venta - Libreria';
    $data['description'] = 'Cargar nueva venta - Libreria';
    $data['page'] = 'venta';

    $data['producto'] = $this->producto_model->getAll("",0,0);
    $data['clientes'] = $this->cliente_model->get_clientes();

    $this->load->view('header', $data);
    $this->load->view('nuevo_venta', $data);
    $this->load->view('footer_venta', $data);
  }

  public function procesar_venta(){
    if($this->input->post('post') && $this->input->post('post')=="1"){
      $this->venta_model->observaciones = $this->input->post('observaciones');
      $this->venta_model->total = $this->input->post('total');
      if ($this->input->post('pago_con') != ''){
        $this->venta_model->pago_con = $this->input->post('pago_con');
      }else{
        $this->venta_model->pago_con = 0;
      }
      $this->venta_model->vuelto = $this->input->post('vuelto');
      $this->venta_model->cuenta_corriente = $this->input->post('cuenta_corriente');
      $this->venta_model->efectivo = $this->input->post('efectivo');
      $this->venta_model->fecha_carga = date("Y-m-d H:i:s");

      $result = $this->venta_model->agregar_venta();

      if ($this->input->post('efectivo') == 'efectivo') {
        $saldoactual = $this->caja_model->get_saldo();

        $this->caja_model->fecha = date("Y-m-d H:i:s");
        $this->caja_model->tipo = 'entrada';
        $this->caja_model->descripcion = 'Venta id: ' . $result;
        $this->caja_model->monto = $this->input->post('total');
        $this->caja_model->saldo = $saldoactual[0]->saldo + $this->input->post('total');

        $this->caja_model->agregar_caja();
      }

      echo $result;
      exit;
    }else{
      $this->load->view('header', $data);
      $this->load->view('nuevo_venta');
      $this->load->view('footer_venta');
    }
  }

  public function procesar_venta_detalle(){
    if($this->input->post('post') && $this->input->post('post')=="1"){
      $this->venta_model->id_venta = $this->input->post('id_venta');
      $this->venta_model->id_producto = $this->input->post('id_producto');
      $this->venta_model->cantidad = $this->input->post('cantidad');
      $this->venta_model->precio_unitario = $this->input->post('precio_unitario');

      $result = $this->venta_model->agregar_venta_detalle();

      /// actualizar stock
      $prod = $this->producto_model->getId($this->input->post('id_producto'));

      $stock = $prod[0]->stock - $this->input->post('cantidad');
      $this->producto_model->actualizarStock($this->input->post('id_producto'), $stock);

      exit;
    }else{
      $this->load->view('header', $data);
      $this->load->view('nuevo_venta');
      $this->load->view('footer_venta');
    }
  }

}
