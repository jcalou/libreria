<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Importar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Importar Datos - Libreria Abracadabra';
        $data['description'] = 'Importar Datos - Libreria Abracadabra';
        $data['page'] = 'importar';

        $this->load->view('header', $data);
        $this->load->view('importar');
        $this->load->view('footer');

    }

    public function excel_upload()
    {
        $this->load->helper(array('form', 'url'));

        $data['title'] = 'Upload de archivos para importacion - Libreria';
        $data['description'] = 'Upload de archivos para importacion - Libreria';
        $data['page'] = 'importar';

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //upload
            $up = $this->upload_files();

            if ( ! $up){
                $data['error'] = array('error' => $this->upload->display_errors());
            }else{
                redirect("importar/excel_read");
            }
            $this->load->view('header', $data);
            $this->load->view('upload', $data);
            $this->load->view('footer');
        }else{
            $this->load->view('header', $data);
            $this->load->view('upload', $data);
            $this->load->view('footer');
        }
    }

    function upload_files(){

        $this->load->library('upload');
        $error = 0;

        for($i=1; $i<2; $i++){

            $_FILES['userfile']['name']     = $_FILES['files'.$i]['name'];
            $_FILES['userfile']['type']     = $_FILES['files'.$i]['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['files'.$i]['tmp_name'];
            $_FILES['userfile']['error']    = $_FILES['files'.$i]['error'];
            $_FILES['userfile']['size']     = $_FILES['files'.$i]['size'];

            $config['upload_path'] = BASEPATH.'../assets/uploads/';
            $config['allowed_types'] = 'gif|jpg|png|xls|xlsx|csv|txt';
            $config['max_size'] = '0';
            $config['overwrite'] = TRUE;
            $config['max_width']  = '0';
            $config['max_height']  = '0';

            $this->upload->initialize($config);

            if($this->upload->do_upload('userfile')){
                $error += 0;
            }else{
                $error += 1;
            }
        }

        if($error > 0){ return FALSE; }else{ return TRUE; }

    }

    public function excel_read()
    {
        set_time_limit(1200); //20 minutos
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('memory_limit','512M');

        $file = BASEPATH.'../assets/uploads/productos.xls';

        //load the excel library
        $this->load->library('excel');

        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);

        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            //header will/should be in row 1 only. of course this can be modified to suit your need.
            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }

        //tranformar excel en tablas para importar

        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
        <html lang="en">
        <head>
            <title>Procesando archivo excel</title>
        </head>
        <body>';
        echo '<h1>Procesando archivo excel</h1>';
        echo '<h3>Ingresando Productos a tabla temporal en base de datos</h3>
        <div id="informationcli" style="width"></div>';

        $i = 1;
        $this->load->model('importar_model');
        $this->importar_model->limpiar_importar_productos();

        foreach ($arr_data as $item) {
            if (isset($item['A'])) {$this->importar_model->p_codigo = $item['A'];}else{$this->importar_model->p_codigo = "";}
            if (isset($item['B'])) {$this->importar_model->p_descripcion = $item['B'];}else{$this->importar_model->p_descripcion = "";}
            if (isset($item['C'])) {$this->importar_model->p_adicional = $item['C'];}else{$this->importar_model->p_adicional = "";}
            if (isset($item['D'])) {$this->importar_model->p_cantidad = strval($item['D']);}else{$this->importar_model->p_cantidad = "";}
            if (isset($item['E'])) {$this->importar_model->p_precio = number_format($item['E'],2,'.','');}else{$this->importar_model->p_precio = "";}
            if (isset($item['F'])) {$this->importar_model->p_iva = number_format($item['F'],2,'.','');}else{$this->importar_model->p_iva = "";}
            if (isset($item['G'])) {$this->importar_model->p_final = number_format($item['G'],2,'.','');}else{$this->importar_model->p_final = "";}
            $this->importar_model->p_check = "1";

            $this->importar_model->agregar_producto();

            // Javascript for updating the progress bar and information
            echo '<script language="javascript">
            document.getElementById("informationcli").innerHTML="'.$i.' filas procesadas.";
            </script>';
            $i++;
        }

        echo "<br>Proceso Terminado con exito!!! Ahora se puede proceder al proceso de importacion,";
        // echo '</body></html>';

        $this->productos_excel();

        exit;

    }

    public function productos_excel()
    {
        echo '<h1>Ingresando a tabla definitiva</h1>
            <h3>Ingresando productos a la tabla definitiva, comparando con existentes</h3>
            <div id="progress" style="width:600px;border:1px solid #ccc;"></div>
            <div id="information" style="width"></div><br>
            ';
        $this->load->model('producto_model');
        $producto_importar = $this->producto_model->get_productos_import();
        $total = count($producto_importar);
        $i = 1;
        $actualizados = $agregados = 0;

        //recorro conexiones a importar
        foreach ($producto_importar as $prod){
            $producto_a_cambiar = $this->producto_model->get_producto_import($prod->codigo);
            if(sizeof($producto_a_cambiar) > 0){
                //Si existe se modifica

              if ($producto_a_cambiar[0]->costo != $prod->final) {
                // cambio el precio
                echo $prod->descripcion . ' cambio el costo de $' . $producto_a_cambiar[0]->costo . ' a $' . $prod->final . '<br>';
                $this->producto_model->siniva = $prod->precio;
                $this->producto_model->iva = $prod->iva;
                $this->producto_model->costo = $prod->final;
                $this->producto_model->margen = $producto_a_cambiar[0]->margen;
                $precio_ = $prod->final + ($prod->final * $producto_a_cambiar[0]->margen) / 100;
                $this->producto_model->precio = number_format($precio_, 2, '.', ',');
              }else {
                $this->producto_model->siniva = $producto_a_cambiar[0]->siniva;
                $this->producto_model->iva = $producto_a_cambiar[0]->iva;
                $this->producto_model->costo = $producto_a_cambiar[0]->costo;
                $this->producto_model->margen = $producto_a_cambiar[0]->margen;
                $this->producto_model->precio = $producto_a_cambiar[0]->precio;
              }

              $this->producto_model->id = $producto_a_cambiar[0]->id;
              $this->producto_model->codigo = $producto_a_cambiar[0]->codigo;
              $this->producto_model->nombre = $prod->descripcion . ' ' . $prod->adicional;
              $this->producto_model->tipo = $producto_a_cambiar[0]->tipo;
              $this->producto_model->subtipo = $producto_a_cambiar[0]->subtipo;
              $this->producto_model->id_proveedor = $producto_a_cambiar[0]->id_proveedor;
              $this->producto_model->codigo_de_barra = $producto_a_cambiar[0]->codigo_de_barra;
              $this->producto_model->marca = $producto_a_cambiar[0]->marca;
              $this->producto_model->descripcion = $producto_a_cambiar[0]->descripcion;
              $this->producto_model->stock = $producto_a_cambiar[0]->stock;
              $this->producto_model->stock_minimo = $producto_a_cambiar[0]->stock_minimo;
              $this->producto_model->ultima_edicion = date('Y-m-d H:i:s');

              $result = $this->producto_model->editar_producto($producto_a_cambiar[0]->id);
              $actualizados += 1;
            }else{
              //Si no existe se agrega
              echo 'Codgo: ' . $prod->codigo . ' - Nombre: ' . $prod->descripcion . ' es un producto nuevo.<br>';
              $this->producto_model->codigo = $prod->codigo;
              $this->producto_model->nombre = $prod->descripcion . ' ' . $prod->adicional;;
              $this->producto_model->tipo = 0;
              $this->producto_model->subtipo = 0;
              $this->producto_model->id_proveedor = 0;
              $this->producto_model->codigo_de_barra = '';
              $this->producto_model->marca = '';
              $this->producto_model->descripcion = $prod->descripcion;
              $this->producto_model->stock = $prod->cantidad;
              $this->producto_model->stock_minimo = 0;
              $this->producto_model->siniva = $prod->precio;
              $this->producto_model->iva = $prod->iva;
              $this->producto_model->costo = $prod->final;
              $this->producto_model->margen = '70';
              $this->producto_model->precio = $prod->final * 1.7;
              $this->producto_model->fecha_carga = date('Y-m-d H:i:s');
              $this->producto_model->ultima_edicion = date('Y-m-d H:i:s');

              $result = $this->producto_model->agregar_producto();
              $agregados += 1;
            }
            // Calculate the percentation
            $percent = intval($i/$total * 100)."%";
            // Javascript for updating the progress bar and information
            echo '<script language="javascript">
            document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>";
            document.getElementById("information").innerHTML="'.$i.' productos procesados.";
            </script>';
            $i++;
        }
        echo "<br>Total de productos Agregados: ". $agregados;
        echo "<br>Total de productos Actualizados: " . $actualizados;
        echo "<br>Proceso Terminado con exito!!!";
        echo '</body></html>';
        return true;
    }


    public function backupdb() {

        set_time_limit(1200); //20 minutos
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        ini_set('memory_limit','512M');

        // Load the DB utility class
        $this->load->dbutil();
        echo "Inicio del proceso<br>";
        echo "Realizar backup Abracadabra<br>";
        // Backup your entire database and assign it to a variable
        $backup =& $this->dbutil->backup();

        echo "Guardar archivo en C:/wamp/www/<br>";
        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file('C:/wamp/www/'.date('d-m-Y').'.gz', $backup);

        echo "Realizar backup<br>";

        $dbhost = 'localhost';
        $dbuser = 'root';
        $dbpass = '';
        $dbname = 'gestion';
        $tables = '*';

        // check mysqli extension installed
        if( ! function_exists('mysqli_connect') ) {
            die(' This scripts need mysql extension to be running properly ! please resolve!!');
        }

        $mysqli = @new mysqli($dbhost, $dbuser, $dbpass, $dbname);

        if( $mysqli->connect_error ) {
            print_r( $mysqli->connect_error );
            return false;
        }

        $res = true;

        $n = 1;
        if( $res ) {

            $name = 'bkp116'.date('d-m-Y');
            $dir = '/var/www/html/support/consultas/assets/uploads';
            # counts
            if( file_exists($dir.'/'.$name.'.sql.gz' ) ) {

              for($i=1;@count( file($dir.'/'.$name.'_'.$i.'.sql.gz') );$i++){
                $name = $name;
                if( ! file_exists( $dir.'/'.$name.'_'.$i.'.sql.gz') ) {
                  $name = $name.'_'.$i;
                  break;
                }
              }
            }

            $fullname = $dir.'/'.$name.'.sql.gz'; # full structures

            if( ! $mysqli->error ) {
                $sql = "SHOW TABLES";
                $show = $mysqli->query($sql);
                $tables = array();
                while ( $r = $show->fetch_array() ) {
                    $tables[] = $r[0];
                }

                if( ! empty( $tables ) ) {

                    //cycle through
                    $return = '';
                    foreach( $tables as $table ) {
                        $result     = $mysqli->query('SELECT * FROM '.$table);
                        $num_fields = $result->field_count;
                        $row2       = $mysqli->query('SHOW CREATE TABLE '.$table );

                        $row2       = $row2->fetch_row();
                        $return    .=
                        "\n
                        -- ---------------------------------------------------------
                        --
                        -- Table structure for table : `{$table}`
                        --
                        -- ---------------------------------------------------------

                        ".$row2[1].";\n";

                        for ($i = 0; $i < $num_fields; $i++) {

                            $n = 1 ;
                            while( $row = $result->fetch_row() ) {
                                if( $n++ == 1 ) { # set the first statements
                                    $return .="
                                        --
                                        -- Dumping data for table `{$table}`
                                        --

                                        ";
                                    /**
                                    * Get structural of fields each tables
                                    */
                                    $array_field = array(); #reset ! important to resetting when loop
                                    while( $field = $result->fetch_field() ) {
                                        $array_field[] = '`'.$field->name.'`';
                                    }
                                    $array_f[$table] = $array_field;
                                    $array_field = implode(', ', $array_f[$table]); #implode arrays
                                    $return .= "INSERT INTO `{$table}` ({$array_field}) VALUES\n(";
                                } else {
                                  $return .= '(';
                                }

                                for($j=0; $j<$num_fields; $j++) {
                                  $row[$j] = str_replace('\'','\'\'', preg_replace("/\n/","\\n", $row[$j] ) );
                                  if ( isset( $row[$j] ) ) { $return .= is_numeric( $row[$j] ) ? $row[$j] : '\''.$row[$j].'\'' ; } else { $return.= '\'\''; }
                                  if ($j<($num_fields-1)) { $return.= ', '; }
                                }
                                $return.= "),\n";
                            }
                            # check matching
                            @preg_match("/\),\n/", $return, $match, false, -3); # check match
                            if( isset( $match[0] ) ) {
                                $return = substr_replace( $return, ";\n", -2);
                            }

                        }

                        $return .= "\n";

                    }

                    $return =
                        "-- ---------------------------------------------------------
                        --
                        -- SIMPLE SQL Dump
                        --
                        -- http://www.nawa.me/
                        --
                        -- Host Connection Info: ".$mysqli->host_info."
                        -- Generation Time: ".date('F d, Y \a\t H:i A ( e )')."
                        -- Server version: ".mysql_get_server_info()."
                        -- PHP Version: ".PHP_VERSION."
                        --
                        -- ---------------------------------------------------------\n\n

                        SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
                        SET time_zone = \"+00:00\";


                        /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
                        /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
                        /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
                        /*!40101 SET NAMES utf8 */;
                        ".$return."
                        /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
                        /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
                        /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;";

                    # end values result

                    @ini_set('zlib.output_compression','Off');
                    $gzipoutput = gzencode( $return, 9);

                    if(  @ file_put_contents( $fullname, $gzipoutput  ) ) { # 9 as compression levels
                        $result = $name.'.sql.gz'; # show the name
                    } else { # if could not put file , automaticly you will get the file as downloadable

                        $result = false;
                        // various headers, those with # are mandatory
                        header('Content-Type: application/x-gzip');
                        header("Content-Description: File Transfer");
                        header('Content-Encoding: gzip'); #
                        header('Content-Length: '.strlen( $gzipoutput ) ); #
                        header('Content-Disposition: attachment; filename="'.$name.'.sql.gz'.'"');
                        header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
                        header('Connection: Keep-Alive');
                        header("Content-Transfer-Encoding: ascii");
                        header('Expires: 0');
                        header('Pragma: no-cache');

                        echo $gzipoutput;
                    }
                } else {
                    $result = '<p>Error when executing database query to export.</p>'.$mysqli->error;
                }
            }

        } else {
            $result = '<p>Wrong mysqli input</p>';
        }

        if( $mysqli && ! $mysqli->error ) {
          @$mysqli->close();
        }

        echo $result;

        echo "Fin del proceso";
    }

    private function TrimTrailingZeroes($nbr) {
        return strpos($nbr,'.')!==false ? rtrim(rtrim($nbr,'0'),'.') : $nbr;
    }

}
