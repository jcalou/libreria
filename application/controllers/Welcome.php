<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('caja_model');
  }

	public function index() {

		$data['title'] = 'Administraci&oacute;n - Libreria';
    $data['description'] = 'Administraci&oacute;n - Libreria';
		$data['page'] = "home";

		//$data['cajavalue'] = $this->caja_model->getLast();
    $data['cajavalue'] = 0;

		$this->load->view('header', $data);
		$this->load->view('welcome_message', $data);
		$this->load->view('footer', $data);
	}

}
