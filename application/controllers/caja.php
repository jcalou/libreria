<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Caja extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('caja_model');
  }

  public function listado() {
    $data['title'] = 'Listado de Caja - Libreria';
    $data['description'] = 'Listado de Caja - Libreria';
    $data['page'] = 'cajas';

    $data['cajas'] = $this->caja_model->get_caja_diaria();
    $data['saldoactual'] = $this->caja_model->get_saldo();
    $data['debito'] = $this->caja_model->get_caja_diaria_debito();
    $data['credito'] = $this->caja_model->get_caja_diaria_credito();
    $data['cheque'] = $this->caja_model->get_caja_diaria_cheque();

    $this->load->view('header', $data);
    $this->load->view('listado_caja', $data);
    $this->load->view('footer');

  }

  public function ajuste(){
    $data['title'] = 'Cargar ajuste - Libreria';
    $data['description'] = 'Cargar ajuste - Libreria';
    $data['page'] = 'cajas';

    if($this->input->post('post') && $this->input->post('post')=="1"){

      $saldoactual = $this->caja_model->get_saldo();

      $this->caja_model->fecha = date("Y-m-d H:i:s");
      $this->caja_model->tipo = 'ajuste';
      $this->caja_model->descripcion = $this->input->post('comentario');
      $this->caja_model->monto = $this->input->post('nuevosaldo') - $saldoactual[0]->saldo;
      $this->caja_model->saldo = $this->input->post('nuevosaldo');

      $this->caja_model->agregar_caja();

      redirect('caja/listado');
      exit;
    } else {

      $this->load->view('header', $data);
      $this->load->view('nuevo_ajuste', $data);
      $this->load->view('footer_gastos');
    }
  }

  public function cerrar_diaria(){
    $data['title'] = 'Cerrar caja diaria - Libreria';
    $data['description'] = 'Cerrar caja diaria - Libreria';
    $data['page'] = 'cajas';

    if($this->input->post('post') && $this->input->post('post')=="1"){

      // date("Y-m-d H:i:s");
      $ventas = $this->input->post('ventas');
      $pagoscli = $this->input->post('pagoscli');
      $compras = $this->input->post('compras');
      $pagospro = $this->input->post('pagospro');
      $gastos = $this->input->post('gastos');
      $ajustes = $this->input->post('ajustes');
      $debito = $this->input->post('debito');
      $credito = $this->input->post('credito');
      $cheque = $this->input->post('cheque');

      $saldoactual = $this->caja_model->get_saldo();

      $this->caja_model->fecha = date("Y-m-d H:i:s");
      $this->caja_model->tipo = 'ajuste';
      $this->caja_model->descripcion = "Ajuste por cierre";
      $this->caja_model->monto = 0 - $saldoactual[0]->saldo;
      $this->caja_model->saldo = 0;

      $this->caja_model->agregar_caja();

      redirect('caja/imprimir_cierre/'.$ventas.'/'.$pagoscli.'/'.$compras.'/'.$pagospro.'/'.$gastos.'/'.$ajustes.'/'.$debito.'/'.$credito.'/'.$cheque);

      exit;
    } else {

      $data['cajadiaria'] = $this->caja_model->get_caja_cierre();
      $data['debito'] = $this->caja_model->get_caja_diaria_debito();
      $data['credito'] = $this->caja_model->get_caja_diaria_credito();
      $data['cheque'] = $this->caja_model->get_caja_diaria_cheque();

      $this->load->view('header', $data);
      $this->load->view('nuevo_cierre', $data);
      $this->load->view('footer');
    }
  }

  public function reporte_caja($desde="-",$hasta="-") {
    $data['title'] = 'Reporte de Caja - Libreria';
    $data['description'] = 'Reporte de Caja - Libreria';
    $data['page'] = 'caja';

    if($this->input->post('post') && $this->input->post('post')=="1"){
      //agregar
      $desde = $this->input->post('desde');
      if ($desde==""){$desde="-";}
      $hasta = $this->input->post('hasta');
      if ($hasta==""){$hasta="-";}

      redirect('caja/reporte_caja/'.$desde.'/'.$hasta.'');
      exit;
    }else{

      $data['caja'] = $this->caja_model->get_caja_reporte(rawurldecode($desde),rawurldecode($hasta));

      $data['desde'] = rawurldecode($desde);
      $data['hasta'] = rawurldecode($hasta);

      $this->load->view('header', $data);
      $this->load->view('reporte_caja', $data);
      $this->load->view('footer_gastos');
    }
  }

  public function listado_gastos() {
    $data['title'] = 'Listado de Gastos - Libreria';
    $data['description'] = 'Listado de Gastos - Libreria';
    $data['page'] = 'caja';

    $limit = 10;
    if ($this->uri->segment('3')){
        $offset = $this->uri->segment('3');
    }else{
        $offset = 0;
    }

    $data['gastos'] = $this->caja_model->get_gastos($limit,$offset);
    $total_rows = $this->caja_model->get_gastos_count();
    $this->load->view('header', $data);

    $this->load->library('pagination');
    $config['base_url'] = base_url('caja/listado_gastos');
    $config['total_rows'] = $total_rows[0]->c;
    $config['per_page'] = 10;

    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = 'Inicio';
    $config['first_tag_open'] = '<li class="arrow">';
    $config['first_tag_close'] = '</li>';
    $config['last_link'] = 'Fin';
    $config['last_tag_open'] = '<li class="arrow">';
    $config['last_tag_close'] = '</li>';
    $config['next_link'] = '&raquo;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = '&laquo;';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="current"><a href="">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);
    $data['pags'] = $this->pagination->create_links();

    $this->load->view('listado_gastos', $data);
    $this->load->view('footer');
  }

  public function reporte_gastos($desde="-",$hasta="-",$rubro="-",$subrubro="-") {
    $data['title'] = 'Reporte de Gastos - Libreria';
    $data['description'] = 'Reporte de Gastos - Libreria';
    $data['page'] = 'caja';

    if($this->input->post('post') && $this->input->post('post')=="1"){
      //agregar
      $desde = $this->input->post('desde');
      if ($desde==""){$desde="-";}
      $hasta = $this->input->post('hasta');
      if ($hasta==""){$hasta="-";}
      $rubro = $this->input->post('rubro');
      if ($rubro==""){$rubro="-";}
      $subrubro = $this->input->post('subrubro');
      if ($subrubro==""){$subrubro="-";}

      redirect('caja/reporte_gastos/'.$desde.'/'.$hasta.'/'.$rubro.'/'.$subrubro.'');
      exit;
    }else{

      $data['gastos'] = $this->caja_model->get_gastos_reporte(rawurldecode($desde),rawurldecode($hasta),rawurldecode($rubro),rawurldecode($subrubro));
      $data['rubros'] = $this->caja_model->get_rubros();
      $this->load->view('header', $data);

      $data['desde'] = rawurldecode($desde);
      $data['hasta'] = rawurldecode($hasta);
      $data['rubro'] = rawurldecode($rubro);
      $data['subrubro'] = rawurldecode($subrubro);

      $this->load->view('reporte_gastos', $data);
      $this->load->view('footer_gastos');
    }
  }

  public function informe_gastos($desde="-",$hasta="-",$totales="-"){
    $data['title'] = 'Informe de Gastos - Libreria';
    $data['description'] = 'Informe de Gastos - Libreria';
    $data['page'] = 'caja';

    if($this->input->post('post') && $this->input->post('post')=="1"){
      //agregar
      $desde = $this->input->post('desde');
      if ($desde==""){$desde="-";}
      $hasta = $this->input->post('hasta');
      if ($hasta==""){$hasta="-";}
      $totales = $this->input->post('totales');
      if ($totales==""){$totales="-";}

      if($totales=="on"){
          redirect('caja/informe_gastos_imprimir_totales/'.$desde.'/'.$hasta);
      }else{
          redirect('caja/informe_gastos_imprimir/'.$desde.'/'.$hasta);
      }

      exit;
    }else{

      $this->load->view('header', $data);
      $this->load->view('informe_gastos', $data);
      $this->load->view('footer_gastos');
    }
  }

  public function nuevo_gasto($monto = ''){
    $data['title'] = 'Cargar nuevo Gasto - Libreria';
    $data['description'] = 'Cargar nuevo Gasto - Libreria';
    $data['page'] = 'caja';
    $data['editar'] = 'nuevo';
    $data['desc'] = $this->input->get('desc');
    $data['monto'] = $monto;

    if($this->input->post('post') && $this->input->post('post')=="1"){
      $today = getdate();
      $fecha = $this->input->post('fecha')." ".$today['hours'].":".$today['minutes'].":".$today['seconds'];
      $this->caja_model->gasto_fecha = $fecha;
      $this->caja_model->gasto_rubro = $this->input->post('rubro');
      $this->caja_model->gasto_subrubro = $this->input->post('subrubro');
      $this->caja_model->gasto_descripcion = $this->input->post('descripcion');
      $this->caja_model->gasto_monto = $this->input->post('monto');

      $result = $this->caja_model->agregar_gasto();

      $saldoactual = $this->caja_model->get_saldo();

      $this->caja_model->fecha = date("Y-m-d H:i:s");
      $this->caja_model->tipo = 'salida';
      $this->caja_model->descripcion = 'Gasto id: ' . $result . ' - ' . $this->input->post('descripcion');
      $this->caja_model->monto = $this->input->post('monto');
      $this->caja_model->saldo = $saldoactual[0]->saldo - $this->input->post('monto');

      $this->caja_model->agregar_caja();

      redirect('caja/listado_gastos');
      exit;
    } else {
      $data['rubro'] = "equipamiento";
      $data['rubros'] = $this->caja_model->get_rubros();
      $this->load->view('header', $data);
      $this->load->view('nuevo_gasto', $data);
      $this->load->view('footer_gastos');
    }
  }

  public function eliminar_gasto($id){
      $data['gasto'] = $this->caja_model->eliminar_gasto($id);
      redirect('caja/listado_gastos');
  }

  public function subrubros($rubro,$subrubro='-'){
    $sub = $this->caja_model->get_subrubros($rubro);
    if(count($sub)==0){
      echo "<option value='-'>-</option>";
    }else{
      echo "<option value='-'>Seleccione subrubro</option>";
      for($i=0;$i<count($sub);$i++){
        if ($sub[$i]->nombre==$subrubro){
          echo "<option value='".$sub[$i]->id."' selected='selected'>".$sub[$i]->nombrelargo."</option>";
        }else{
          echo "<option value='".$sub[$i]->id."'>".$sub[$i]->nombrelargo."</option>";
        }
      }
    }
  }

  public function reporte_imprimir($desde="-",$hasta="-",$rubro="-",$subrubro="-",$usuario="-"){

    $gastos = $this->caja_model->get_gastos_reporte(rawurldecode($desde),rawurldecode($hasta),rawurldecode($rubro),rawurldecode($subrubro),rawurldecode($usuario));

    error_reporting(E_ALL);

    $this->load->helper('pdf_helper');
    tcpdf();

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Libreria');
    $pdf->SetTitle('Reporte de Gastos');
    $pdf->SetSubject('Reporte de Gastos');
    $pdf->SetKeywords('libreria, gastos');

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, 14, PDF_MARGIN_RIGHT);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // add a page
    $pdf->AddPage();

    $pdf->SetFont('helvetica', '', 10);

    $tbl = '<h1>Reporte de Gastos</h1>';
    $tbl .= '<h2>Filtros</h2>';
    $tbl .= '<p>Desde: '.rawurldecode($desde).'<br>';
    $tbl .= 'Hasta: '.rawurldecode($hasta).'<br>';
    $tbl .= 'Rubro: '.rawurldecode($rubro).'<br>';
    $tbl .= 'Subrubro: '.rawurldecode($subrubro).'<br>';

    $tbl .= '<table>';
    $tbl .= '<thead><tr><td style="width:23%;">Fecha y hora</td><td style="width:15%;">Monto</td><td style="width:42%;">Descripcion</td><td style="width:10%;">Rubro</td><td style="width:10%;">Subrubro</td></tr></thead><tbody>';

    $totaltotal = 0;

    if (count($gastos)>0){
        for($i=0;$i<count($gastos);$i++){
            $tbl .= '<tr><td style="width:23%;">'.$gastos[$i]->fecha.'</td><td style="width:15%;">$ '.$gastos[$i]->monto.'</td><td style="width:42%;">'.$gastos[$i]->descripcion.'</td><td style="width:10%;>'.$gastos[$i]->rubro.'</td><td style="width:10%;>'.$gastos[$i]->subrubro.'</td>'.'</tr>';
            $totaltotal = $totaltotal + $gastos[$i]->monto;
        }
    }else{
        $tbl .= '<tr><td colspan="2">No se registraron gastos</td></tr>';
    }

    $tbl .= '</tbody></table>';
    $tbl .= '<h2>Total: $ '.$totaltotal.'</h2>';

    $pdf->writeHTML($tbl, true, false, false, false, '');

    //Close and output PDF document
    $pdf->Output('gastos.pdf', 'I');

    exit;

  }

  public function informe_gastos_imprimir($desde="-",$hasta="-"){

    $gastos = $this->caja_model->get_gastos_informe(rawurldecode($desde),rawurldecode($hasta));

    error_reporting(E_ALL);

    $this->load->helper('pdf_helper');

    tcpdf();

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Libreria');
    $pdf->SetTitle('Informe de Gastos');
    $pdf->SetSubject('Informe de Gastos');
    $pdf->SetKeywords('libreria, gastos');

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, 14, PDF_MARGIN_RIGHT);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // add a page
    $pdf->AddPage();

    $pdf->SetFont('helvetica', '', 10);

    $time1 = strtotime(rawurldecode($desde));
    $time2 = strtotime(rawurldecode($hasta));

    $tbl = '<h1>Informe de Gastos - (Desde el '.date("d/m/Y",$time1).' hasta el '.date("d/m/Y",$time2).')</h1>';

    $elem = $gastos[0]->rubro;
    $tbl .= '<h2>Rubro: '.$elem.'</h2>';
    $tbl .= '<table>';
    $tbl .= '<thead><tr><td style="width:15%;">Fecha y hora</td><td style="width:13%;">Monto</td><td style="width:15%;">Subrubro</td><td style="width:57%;">Descripcion</td></tr></thead><tbody>';
    $totaltotal = 0;$totalgeneral = 0;

    for($i=0;$i<count($gastos);$i++){
      if($gastos[$i]->rubro == $elem) {
        $fecha = strtotime($gastos[$i]->fecha);
        $tbl .= '<tr><td style="width:15%;">'.date("d/m H:m",$fecha).'</td><td style="width:13%;">$ '.$gastos[$i]->monto.'</td><td style="width:15%;">'.$gastos[$i]->subrubro.'</td><td style="width:57%;">'.$gastos[$i]->descripcion.'</td></tr>';
        $totaltotal = $totaltotal + $gastos[$i]->monto;
        $totalgeneral = $totalgeneral + $gastos[$i]->monto;
      }else{
        $tbl .= '</tbody></table>';
        $tbl .= '<h3>Total: $ '.$totaltotal.'</h3><hr>';
        $totaltotal = 0;
        $tbl .= '<h2>Rubro: '.$gastos[$i]->rubro.'</h2>';
        $tbl .= '<table>';
        $tbl .= '<thead><tr><td style="width:15%;">Fecha y hora</td><td style="width:13%;">Monto</td><td style="width:15%;">Subrubro</td><td style="width:57%;">Descripcion</td></tr></thead><tbody>';
        $fecha = strtotime($gastos[$i]->fecha);
        $tbl .= '<tr><td style="width:15%;">'.date("d/m H:m",$fecha).'</td><td style="width:13%;">$ '.$gastos[$i]->monto.'</td><td style="width:15%;">'.$gastos[$i]->subrubro.'</td><td style="width:57%;">'.$gastos[$i]->descripcion.'</td></tr>';
        $totaltotal = $totaltotal + $gastos[$i]->monto;
        $totalgeneral = $totalgeneral + $gastos[$i]->monto;
      }
      $elem = $gastos[$i]->rubro;
    }
    $tbl .= '</tbody></table>';
    $tbl .= '<h3>Total: $ '.$totaltotal.'</h3>';
    $tbl .= '<h3>Total General: $ '.$totalgeneral.'</h3>';

    $pdf->writeHTML($tbl, true, false, false, false, '');

    //Close and output PDF document
    $pdf->Output('gastos.pdf', 'I');

    exit;

  }

  public function informe_gastos_imprimir_totales($desde="-",$hasta="-"){

    $gastos = $this->caja_model->get_gastos_informe_totales(rawurldecode($desde),rawurldecode($hasta));

    error_reporting(E_ALL);

    $this->load->helper('pdf_helper');

    tcpdf();

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Libreria');
    $pdf->SetTitle('Informe de Gastos');
    $pdf->SetSubject('Informe de Gastos');
    $pdf->SetKeywords('libreria, gastos');

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, 14, PDF_MARGIN_RIGHT);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // add a page
    $pdf->AddPage();

    $pdf->SetFont('helvetica', '', 11);

    $time1 = strtotime(rawurldecode($desde));
    $time2 = strtotime(rawurldecode($hasta));

    $tbl = '<h2>Informe de Gastos - (Desde el '.date("d/m/Y",$time1).' hasta el '.date("d/m/Y",$time2).')</h2>';

    $elem = $gastos[0]->rubro;
    $subelem = $gastos[0]->subrubro;
    $tbl .= '<p><b>Rubro: '.$elem.'</b><br>';
    $totaltotal = 0;$subtotaltotal = 0;$totalgeneral = 0;

    for($i=0;$i<count($gastos);$i++){
      if($gastos[$i]->rubro == $elem) {
        if($gastos[$i]->subrubro == $subelem) {
          $subtotaltotal = $subtotaltotal + $gastos[$i]->monto;
        }else{
          $tbl .= $subelem.': $ '.$subtotaltotal.'<br>';
          $subtotaltotal = 0;
          $subtotaltotal = $subtotaltotal + $gastos[$i]->monto;
        }
        $totaltotal = $totaltotal + $gastos[$i]->monto;
        $totalgeneral = $totalgeneral + $gastos[$i]->monto;
      }else{
        $tbl .= $subelem.': $ '.$subtotaltotal.'<br>';
        $tbl .= 'Total: $ '.$totaltotal.'</p>';
        $totaltotal = 0;
        $subtotaltotal = 0;
        $tbl .= '<p><b>Rubro: '.$gastos[$i]->rubro.'</b><br>';
        $totaltotal = $totaltotal + $gastos[$i]->monto;
        $totalgeneral = $totalgeneral + $gastos[$i]->monto;
        $subtotaltotal = $subtotaltotal + $gastos[$i]->monto;
        $subelem = "";
      }
      $elem = $gastos[$i]->rubro;
      $subelem = $gastos[$i]->subrubro;
    }
    $tbl .= $subelem.': $ '.$subtotaltotal.'<br>';
    $tbl .= 'Total: $ '.$totaltotal.'</p>';
    $tbl .= '<h3>Total General: $ '.$totalgeneral.'</h3>';

    $pdf->writeHTML($tbl, true, false, false, false, '');

    //Close and output PDF document
    $pdf->Output('gastos.pdf', 'I');

    exit;
  }

  public function imprimir_cierre($ventas, $pagoscli, $compras, $pagospro, $gastos, $ajustes, $debito, $credito, $cheque){

    error_reporting(E_ALL);

    $this->load->helper('pdf_helper');

    tcpdf();

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Libreria');
    $pdf->SetTitle('Cierre diario');
    $pdf->SetSubject('Cierre diario');
    $pdf->SetKeywords('libreria, caja');

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, 14, PDF_MARGIN_RIGHT);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // add a page
    $pdf->AddPage();

    $pdf->SetFont('helvetica', '', 11);


    $tbl = '<h2>Abracadabra - Cierre del dia '.date("d/m/Y").'</h2>';

    $tbl .= '<h3 style="text-decoration:underline;">Ingresos</h3>';
    $tbl .= '<p><b>- Ventas:</b> $ '.$ventas.'</p>';
    $tbl .= '<p><b>- Pagos de clientes:</b> $ '.$pagoscli.'</p>';
    $tbl .= '<h4>Total ingresos:</b> $ '.($ventas + $pagoscli).'</h4><br><br>';

    $tbl .= '<h3 style="text-decoration:underline;">Egresos</h3>';
    $tbl .= '<p><b>- Compras:</b> $ '.$compras.'</p>';
    $tbl .= '<p><b>- Pago a proveedores:</b> $ '.$pagospro.'</p>';
    $tbl .= '<p><b>- Gastos:</b> $ '.$gastos.'</p><br><br>';
    $tbl .= '<h4>Total egresos:</b> $ '.($compras + $pagospro + $gastos).'</h4><br><br>';

    $tbl .= '<h3 style="text-decoration:underline;">Otros Movimientos</h3>';
    $tbl .= '<p><b>- Ajustes:</b> $ '.$ajustes.'</p>';

    $tbl .= '<h2>Total general (Ingresos - Egresos + Ajustes): $ '.(($ventas + $pagoscli)-($compras + $pagospro + $gastos)+($ajustes)).'</h2>';

    $tbl .= '<h3 style="text-decoration:underline;">Otros Medios de Pago</h3>';
    $tbl .= '<p><b>- Tarjeta de Debito:</b> $ '.$debito.'</p>';
    $tbl .= '<p><b>- Tarjeta de Credito:</b> $ '.$credito.'</p>';
    $tbl .= '<p><b>- Cheque:</b> $ '.$cheque.'</p>';

    $tbl .= '<h2>Total otros medios (Debito + Credito + Cheque): $ '.($debito + $credito + $cheque).'</h2>';


    $pdf->writeHTML($tbl, true, false, false, false, '');

    //Close and output PDF document
    $pdf->Output('cierre.pdf', 'I');

    exit;
  }

}
