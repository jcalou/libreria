<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Producto extends CI_Controller{
    public function __construct(){
        //Cargamos El Constructor
        parent::__construct();
        $this->load->model('producto_model');
        $this->load->model('proveedor_model');

    }

    public function listado()
    {
        $data['title'] = 'Listado de Productos - Libreria';
        $data['description'] = 'Listado de Productos - Libreria';
        $data['page'] = 'productos';

        if($this->input->get('q')) {
            $data['productos'] = $this->producto_model->getAll($this->input->get('q'),'','');
            $data['q'] = $this->input->get('q');

            $this->load->view('header', $data);
            $this->load->view('listado_producto', $data);
            $this->load->view('footer');
        }else{
            $limit = 10;
            if ($this->uri->segment('3')){
                $offset = $this->uri->segment('3');
            }else{
                $offset = 0;
            }
            $data['productos'] = $this->producto_model->getAll('_all',$limit,$offset);
            $this->load->library('pagination');
            $config['base_url'] = base_url('producto/listado');
            $total_rows = $this->producto_model->getAllCount();
            $config['total_rows'] = $total_rows[0]->c;
            $config['per_page'] = 10;

            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = 'Inicio';
            $config['first_tag_open'] = '<li class="arrow">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'Fin';
            $config['last_tag_open'] = '<li class="arrow">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = '&raquo;';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = '&laquo;';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="current"><a href="">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $data['pags'] = $this->pagination->create_links();

            $this->load->view('header', $data);
            $this->load->view('listado_producto', $data);
            $this->load->view('footer');
        }
    }

    public function listado_stock_minimo()
    {
        $data['title'] = 'Listado de Productos - Libreria';
        $data['description'] = 'Listado de Productos - Libreria';
        $data['page'] = 'productos';

        $data['productos'] = $this->producto_model->getStockMinimo();

        $this->load->view('header', $data);
        $this->load->view('listado_producto_stock_minimo', $data);
        $this->load->view('footer');
    }

    public function listado_stock_negativo()
    {
        $data['title'] = 'Listado de Productos - Libreria';
        $data['description'] = 'Listado de Productos - Libreria';
        $data['page'] = 'productos';

        $data['productos'] = $this->producto_model->getStockNegativo();

        $this->load->view('header', $data);
        $this->load->view('listado_producto_stock_negativo', $data);
        $this->load->view('footer');
    }

    public function nuevo(){

        $data['title'] = 'Cargar nuevo Producto - Libreria';
        $data['description'] = 'Cargar nuevo Producto - Libreria';
        $data['page'] = 'productos';
        $data['editar'] = 'nuevo';

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $this->producto_model->nombre = $this->input->post('nombre');
            $this->producto_model->tipo = $this->input->post('rubro');
            $this->producto_model->subtipo = $this->input->post('subtipo');
            $this->producto_model->id_proveedor = $this->input->post('proveedores');
            $this->producto_model->codigo_de_barra = $this->input->post('codigo_de_barra');
            $this->producto_model->marca = $this->input->post('marca');
            $this->producto_model->descripcion = $this->input->post('descripcion');
            $this->producto_model->stock = $this->input->post('stock');
            $this->producto_model->stock_minimo = $this->input->post('stock_minimo');
            $this->producto_model->costo = $this->input->post('costo');
            $this->producto_model->margen = $this->input->post('margen');
            $this->producto_model->precio = $this->input->post('precio');
            $this->producto_model->fecha_carga = date('Y-m-d H:i:s');
            $this->producto_model->ultima_edicion = date('Y-m-d H:i:s');

            $result = $this->producto_model->agregar_producto();
            redirect('producto/listado');
            exit;
        }else{
            $data['rubro'] = '';
            $data['rubros'] = $this->producto_model->get_rubros();
            $data['proveedores'] = $this->proveedor_model->get_proveedores();
            $data['id_proveedor'] = '';

            $this->load->view('header', $data);
            $this->load->view('nuevo_producto', $data);
            $this->load->view('footer_producto', $data);
        }
    }

    public function editar($id)
    {
        $data['title'] = 'Editar Producto - Libreria';
        $data['description'] = 'Editar Producto - Libreria';
        $data['page'] = 'productos';
        $data['editar'] = 'editar';
        $data['producto'] = $this->producto_model->getId($id);

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //editar
            $this->producto_model->nombre = $this->input->post('nombre');
            $this->producto_model->tipo = $this->input->post('rubro');
            $this->producto_model->subtipo = $this->input->post('subtipo');
            $this->producto_model->id_proveedor = $this->input->post('proveedores');
            $this->producto_model->codigo_de_barra = $this->input->post('codigo_de_barra');
            $this->producto_model->marca = $this->input->post('marca');
            $this->producto_model->descripcion = $this->input->post('descripcion');
            $this->producto_model->stock = $this->input->post('stock');
            $this->producto_model->stock_minimo = $this->input->post('stock_minimo');
            $this->producto_model->costo = $this->input->post('costo');
            $this->producto_model->margen = $this->input->post('margen');
            $this->producto_model->precio = $this->input->post('precio');
            $this->producto_model->ultima_edicion = date('Y-m-d H:i:s');

            $result = $this->producto_model->editar_producto($id);
            redirect('producto/listado');
        }else{
            $data['rubro'] = $data['producto'][0]->tipo;
            $data['rubros'] = $this->producto_model->get_rubros();
            $data['proveedores'] = $this->proveedor_model->get_proveedores();
            $data['id_proveedor'] = $data['producto'][0]->id_proveedor;

            $this->load->view('header', $data);
            $this->load->view('nuevo_producto', $data);
            $this->load->view('footer_producto', $data);
        }
    }

    public function eliminar($id)
    {
        $data['producto'] = $this->producto_model->eliminar_producto($id);
        redirect('producto/listado');
    }

    public function subrubros($rubro,$subrubro='-')
    {
        $sub = $this->producto_model->get_subrubros($rubro);
        if(count($sub)==0){
            echo "<option value='-'>-</option>";
        }else{
            echo "<option value='-'>Seleccione subrubro</option>";
            for($i=0;$i<count($sub);$i++){
                if ($sub[$i]->id==$subrubro){
                    echo "<option value='".$sub[$i]->id."' selected='selected'>".$sub[$i]->nombre."</option>";
                }else{
                    echo "<option value='".$sub[$i]->id."'>".$sub[$i]->nombre."</option>";
                }
            }
        }
    }

    public function redondearprecios() {
      // redondear a 0 centavos precios mayores a 2 pesos
      $this->producto_model->redondearprecios();
      redirect('producto/listado');
    }

}
