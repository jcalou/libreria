<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagoproveedores extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('pagoproveedores_model');
    $this->load->model('caja_model');
    $this->load->model('proveedor_model');
  }

  public function listado() {
    $data['title'] = 'Listado de Pagos a proveedores - Libreria';
    $data['description'] = 'Listado de Pagos a proveedores - Libreria';
    $data['page'] = 'pagoproveedores';

    if($this->input->get('q')) {
      $data['pagos'] = $this->pagoproveedores_model->buscar_pagos($this->input->get('q'));
      $data['q'] = $this->input->get('q');

      $this->load->view('header', $data);
      $this->load->view('listado_pagoproveedores', $data);
      $this->load->view('footer');
    }else{
      $data['pagos'] = $this->pagoproveedores_model->get_pagos();

      $this->load->view('header', $data);
      $this->load->view('listado_pagoproveedores', $data);
      $this->load->view('footer');
    }
  }

  public function ingresar(){

    $data['title'] = 'Cargar nueva pago a proveedores - Libreria';
    $data['description'] = 'Cargar nueva pago a proveedores - Libreria';
    $data['page'] = 'pagoproveedores';
    $data['editar'] = 'nuevo';

    if($this->input->post('post') && $this->input->post('post')=="1"){
        //agregar
        $this->pagoproveedores_model->id_proveedor = $this->input->post('proveedores');
        $this->pagoproveedores_model->fecha = $this->input->post('fecha'). ' ' . date('H:i:s');
        $this->pagoproveedores_model->monto = $this->input->post('monto');
        $this->pagoproveedores_model->comentario = $this->input->post('comentario');

        $result = $this->pagoproveedores_model->agregar_pago();

        $saldoactual = $this->caja_model->get_saldo();

        $this->caja_model->fecha = date("Y-m-d H:i:s");
        $this->caja_model->tipo = 'salida';
        $this->caja_model->descripcion = 'Pago a proovedor id: ' . $result;
        $this->caja_model->monto = $this->input->post('monto');
        $this->caja_model->saldo = $saldoactual[0]->saldo - $this->input->post('monto');

        $this->caja_model->agregar_caja();

        redirect('pagoproveedores/listado');
        exit;
    }else{

      $data['proveedores'] = $this->proveedor_model->get_proveedores();

      $this->load->view('header', $data);
      $this->load->view('nuevo_pagoproveedores', $data);
      $this->load->view('footer_gastos', $data);
    }
  }

}
