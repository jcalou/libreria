<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compra extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('compra_model');
    $this->load->model('caja_model');
    $this->load->model('pagoproveedores_model');
    $this->load->model('producto_model');
    $this->load->model('proveedor_model');
  }

  public function listado() {
    $data['title'] = 'Listado de Compras - Libreria';
    $data['description'] = 'Listado de Compras - Libreria';
    $data['page'] = 'compra';

    if($this->input->get('q')) {
      $data['compras'] = $this->compra_model->buscar_compras($this->input->get('q'));
      $data['q'] = $this->input->get('q');

      $this->load->view('header', $data);
      $this->load->view('listado_compra', $data);
      $this->load->view('footer');
    }else{
      $data['compras'] = $this->compra_model->get_compras();

      $this->load->view('header', $data);
      $this->load->view('listado_compra', $data);
      $this->load->view('footer');
    }
  }

  public function cuenta_proveedor($id = 0) {
    $data['title'] = 'Cuenta corriente del proveedor - Libreria';
    $data['description'] = 'Cuenta corriente del proveedor - Libreria';
    $data['page'] = 'compra';

    $data['compras'] = $this->compra_model->buscar_compras_cc($id);
    $data['pagosproveedores'] = $this->pagoproveedores_model->buscar_pagos($id);
    $data['id'] = $id;

    $this->load->view('header', $data);
    $this->load->view('cuenta_corriente_compra', $data);
    $this->load->view('footer');

  }

  public function ingresar(){
    $data['title'] = 'Cargar nueva compra - Libreria';
    $data['description'] = 'Cargar nueva compra - Libreria';
    $data['page'] = 'compra';

    $data['producto'] = $this->producto_model->getAll("",0,0);
    $data['proveedores'] = $this->proveedor_model->get_proveedores();

    $this->load->view('header', $data);
    $this->load->view('nuevo_compra', $data);
    $this->load->view('footer_compra', $data);
  }

  public function procesar_compra(){
    if($this->input->post('post') && $this->input->post('post')=="1"){
      $this->compra_model->observaciones = $this->input->post('observaciones');
      $this->compra_model->total = $this->input->post('total');
      if ($this->input->post('pago_con') != ''){
        $this->compra_model->pago_con = $this->input->post('pago_con');
      }else{
        $this->compra_model->pago_con = 0;
      }
      $this->compra_model->vuelto = $this->input->post('vuelto');
      $this->compra_model->cuenta_corriente = $this->input->post('cuenta_corriente');
      $this->compra_model->efectivo = $this->input->post('efectivo');
      $this->compra_model->fecha_carga = date("Y-m-d H:i:s");

      $result = $this->compra_model->agregar_compra();

      if ($this->input->post('efectivo') == 'efectivo') {
        $saldoactual = $this->caja_model->get_saldo();

        $this->caja_model->fecha = date("Y-m-d H:i:s");
        $this->caja_model->tipo = 'salida';
        $this->caja_model->descripcion = 'Comrpa id: ' . $result;
        $this->caja_model->monto = $this->input->post('total');
        $this->caja_model->saldo = $saldoactual[0]->saldo - $this->input->post('total');

        $this->caja_model->agregar_caja();
      }
      echo $result;
      exit;
    }else{
      $this->load->view('header', $data);
      $this->load->view('nuevo_compra');
      $this->load->view('footer_compra');
    }
  }

  public function procesar_compra_detalle(){
    if($this->input->post('post') && $this->input->post('post')=="1"){
      $this->compra_model->id_compra = $this->input->post('id_compra');
      $this->compra_model->id_producto = $this->input->post('id_producto');
      $this->compra_model->cantidad = $this->input->post('cantidad');
      $this->compra_model->precio_unitario = $this->input->post('precio_unitario');

      $result = $this->compra_model->agregar_compra_detalle();

      /// actualizar stock
      $prod = $this->producto_model->getId($this->input->post('id_producto'));

      $stock = $prod[0]->stock - $this->input->post('cantidad');
      $this->producto_model->actualizarStock($this->input->post('id_producto'), $stock);

      exit;
    }else{
      $this->load->view('header', $data);
      $this->load->view('nuevo_compra');
      $this->load->view('footer_compra');
    }
  }

}
