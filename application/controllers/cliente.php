<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cliente extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        $this->load->model('cliente_model');
    }

    public function listado()
    {
        $data['title'] = 'Listado de Clientes - Libreria';
        $data['description'] = 'Listado de Clientes - Libreria';
        $data['page'] = 'clientes';

        if($this->input->get('q')) {
            $data['clientes'] = $this->cliente_model->buscar_clientes($this->input->get('q'));
            $data['q'] = $this->input->get('q');

            $this->load->view('header', $data);
            $this->load->view('listado_cliente', $data);
            $this->load->view('footer');
        }else{

            $data['clientes'] = $this->cliente_model->get_clientes();

            $this->load->view('header', $data);
            $this->load->view('listado_cliente', $data);
            $this->load->view('footer');
        }

    }

    public function nuevo() {

        $data['title'] = 'Cargar nuevo Cliente - Libreria';
        $data['description'] = 'Cargar nuevo Cliente - Libreria';
        $data['page'] = 'clientes';
        $data['editar'] = 'nuevo';

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $this->cliente_model->nombre = $this->input->post('nombre');
            $this->cliente_model->direccion = $this->input->post('direccion');
            $this->cliente_model->telefono = $this->input->post('telefono');
            $this->cliente_model->contacto = $this->input->post('contacto');
            $this->cliente_model->email = $this->input->post('email');
            $this->cliente_model->cuit = $this->input->post('cuit');
            $this->cliente_model->observaciones = $this->input->post('observaciones');
            $this->cliente_model->fecha_carga = date("Y-m-d H:i:s");
            $this->cliente_model->ultima_edicion = date("Y-m-d H:i:s");

            $result = $this->cliente_model->agregar_cliente();
            redirect('cliente/listado');
            exit;
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_cliente');
            $this->load->view('footer');
        }
    }

    public function editar($id) {
        $data['title'] = 'Editar Cliente - Libreria';
        $data['description'] = 'Editar Cliente - Libreria';
        $data['page'] = 'clientes';
        $data['editar'] = 'editar';
        $data['cliente'] = $this->cliente_model->get_cliente($id);

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //editar
            $this->cliente_model->nombre = $this->input->post('nombre');
            $this->cliente_model->direccion = $this->input->post('direccion');
            $this->cliente_model->telefono = $this->input->post('telefono');
            $this->cliente_model->contacto = $this->input->post('contacto');
            $this->cliente_model->email = $this->input->post('email');
            $this->cliente_model->cuit = $this->input->post('cuit');
            $this->cliente_model->observaciones = $this->input->post('observaciones');
            $this->cliente_model->ultima_edicion = date("Y-m-d H:i:s");

            $result = $this->cliente_model->editar_cliente($id);
            redirect('cliente/listado');
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_cliente');
            $this->load->view('footer');
        }
    }

    public function eliminar($id)
    {
        $data['cliente'] = $this->cliente_model->eliminar_cliente($id);
        redirect('cliente/listado');
    }

}
