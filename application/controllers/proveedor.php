<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proveedor extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        $this->load->model('proveedor_model');
    }

    public function listado()
    {
        $data['title'] = 'Listado de Proveedores - Libreria';
        $data['description'] = 'Listado de Proveedores - Libreria';
        $data['page'] = 'proveedores';

        if($this->input->get('q')) {
            $data['proveedores'] = $this->proveedor_model->buscar_proveedores($this->input->get('q'));
            $data['q'] = $this->input->get('q');

            $this->load->view('header', $data);
            $this->load->view('listado_proveedor', $data);
            $this->load->view('footer');
        }else{

            $data['proveedores'] = $this->proveedor_model->get_proveedores();

            $this->load->view('header', $data);
            $this->load->view('listado_proveedor', $data);
            $this->load->view('footer');
        }

    }

    public function nuevo() {

        $data['title'] = 'Cargar nuevo Proveedor - Libreria';
        $data['description'] = 'Cargar nuevo Proveedor - Libreria';
        $data['page'] = 'proveedores';
        $data['editar'] = 'nuevo';

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $this->proveedor_model->nombre = $this->input->post('nombre');
            $this->proveedor_model->direccion = $this->input->post('direccion');
            $this->proveedor_model->telefono = $this->input->post('telefono');
            $this->proveedor_model->contacto = $this->input->post('contacto');
            $this->proveedor_model->email = $this->input->post('email');
            $this->proveedor_model->cuit = $this->input->post('cuit');
            $this->proveedor_model->observaciones = $this->input->post('observaciones');
            $this->proveedor_model->fecha_carga = date("Y-m-d H:i:s");
            $this->proveedor_model->ultima_edicion = date("Y-m-d H:i:s");

            $result = $this->proveedor_model->agregar_proveedor();
            redirect('proveedor/listado');
            exit;
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_proveedor');
            $this->load->view('footer');
        }
    }

    public function editar($id) {
        $data['title'] = 'Editar Proveedor - Libreria';
        $data['description'] = 'Editar Proveedor - Libreria';
        $data['page'] = 'proveedores';
        $data['editar'] = 'editar';
        $data['proveedor'] = $this->proveedor_model->get_proveedor($id);

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //editar
            $this->proveedor_model->nombre = $this->input->post('nombre');
            $this->proveedor_model->direccion = $this->input->post('direccion');
            $this->proveedor_model->telefono = $this->input->post('telefono');
            $this->proveedor_model->contacto = $this->input->post('contacto');
            $this->proveedor_model->email = $this->input->post('email');
            $this->proveedor_model->cuit = $this->input->post('cuit');
            $this->proveedor_model->observaciones = $this->input->post('observaciones');
            $this->proveedor_model->ultima_edicion = date("Y-m-d H:i:s");

            $result = $this->proveedor_model->editar_proveedor($id);
            redirect('proveedor/listado');
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_proveedor');
            $this->load->view('footer');
        }
    }

    public function eliminar($id)
    {
        $data['proveedor'] = $this->proveedor_model->eliminar_proveedor($id);
        redirect('proveedor/listado');
    }

}
