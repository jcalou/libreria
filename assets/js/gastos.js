$(document).ready(function(){

  // logica para reporte de gastos
  $('#reporte-gastos-dp1').fdatepicker({
    format: 'yyyy-mm-dd',
    language: 'es'
  });
  $('#reporte-gastos-dp2').fdatepicker({
    format: 'yyyy-mm-dd',
    language: 'es'
  });
  var aug = new Date('2015', '07', '01', 0, 0, 0, 0);
  $('#reporte-gastos-dp3').fdatepicker({
    format: 'yyyy-mm-dd',
    language: 'es',
    onRender: function (date) {
      return date.valueOf() <= aug.valueOf() ? 'disabled' : '';
    }
  });
  // fin logica para reporte de gastos

  // dropdown rubro - subrubro
    $("#rubro").change(function() {
        $.ajax({
          url: ipsiteurl + "libreria/caja/subrubros/" + $(this).val()
        })
        .success(function(data) {
            $("#subrubro").html(data);
        });
    });
  // dropdown rubro - subrubro

  setTimeout(function(){
    $.ajax({
      url: ipsiteurl + "libreria/caja/subrubros/" + $('#rubro').val() + '/' + subrubro
    })
    .success(function(data) {
        $("#subrubro").html(data);
    });
  },100);

});

