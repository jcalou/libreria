function updateTotal() {
  var nuevototal = 0;
  $('#items').find('tbody tr').each(function(index, el) {
    var precio = $(this).children('td.item_preciototal').text().replace('$', '');
    var precionum = parseFloat(precio);

    nuevototal = nuevototal + precionum;
  });
  $('#total').text('$ ' + nuevototal.toFixed(2));
  $('#total_findeventa').text('$ ' + nuevototal.toFixed(2));

  var clien = $('#cliente option:selected').text();
  var pago = '';
  if ($("#checkbox1").is(":checked")) pago = ' (PAGO EN EFECTIVO)';
  if (clien == 'Seleccione cliente') clien = '';
  if (clien != '') clien = clien + pago;
  $('#cliente_findeventa').text(clien);

  return 0;
}

function rebindRemove () {
  $(".item_remove").on('click', function(e) {
    e.preventDefault();
    var row = $(this).parent('td').parent('tr');
    row.remove();
    updateTotal();
  });
}

$(document).ready(function(){

  $('#cliente').on('change', function() {
    updateTotal();
  });

  $("#q").autocomplete({
    source: availableTags,
    selectFirst: true,
    delay: 0,
    autoFocus: true,
    minLength: 3,
    select: function( event, ui ) {

      var precio = parseFloat(ui.item.precio).toFixed(2);
      var cant = parseFloat($("#cantidad").val());

      $("#preciounitario").val(precio);
      $("#preciototal").val((precio*cant).toFixed(2));

      $("#id_hidden").val(ui.item.id);

      $("#q").val(ui.item.label);

      if (event.keyCode == '13') {
        $('#agregaritem').click();
      }

      return false;
    }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label + " - $" + item.precio + "</a>" )
        .appendTo( ul );
    };

    $("#q").keypress(function(e) {
      if (e.which == '13') {
        e.preventDefault();
        $('#agregaritem').click();
      }
    });

    $("#qbc").keypress(function(e) {
      if (e.which == '13') {
        e.preventDefault();
        //buscar por codigo de barra
        var result = $.grep(availableTags, function(e){ return e.bc == $("#qbc").val(); });

        if (result.length == 0) {
          alert('Producto no encontrado');
          $("#q").val("");
          $("#qbc").val("");
          $("#qbc-name").val("");
          $("#preciounitario").val("");
          $("#preciototal").val("");
        } else if (result.length == 1) {
          // access the foo property using result[0].foo
          var precio = parseFloat(result[0].precio).toFixed(2);
          var cant = parseFloat($("#cantidad").val());

          $("#qbc-name").val(result[0].nombre);
          $("#preciounitario").val(precio);
          $("#preciototal").val((precio*cant).toFixed(2));

          $("#id_hidden").val(result[0].id);

          //agregar item
          $('#agregaritem').click();

          $("#qbc").focus();
        } else {
          alert('Hay mas de un producto con ese codigo. Revise los productos.');
          $("#q").val("");
          $("#qbc").val("");
          $("#qbc-name").val("");
          $("#preciounitario").val("");
          $("#preciototal").val("");
        }


      }
    });

    $('#bc-image').click(function(event) {
      $("#qbc").focus();
    });

  // limpiar el item
  $("#limpiaritem").click(function(event) {
    event.preventDefault();
    $("#q").val("");
    $("#qbc").val("");
    $("#qbc-name").val("");
    $("#preciounitario").val("");
    $("#preciototal").val("");
    $("#q").focus();
  });

  // recalcular precio en base a cantidad
  $("#cantidad").bind('change', function(event) {
    event.preventDefault();
    var precio = parseFloat($("#preciounitario").val()).toFixed(2);
    if (precio > 0) {
      var cant = parseFloat($("#cantidad").val());
      $("#preciototal").val((precio*cant).toFixed(2));
    }
  });

  // recalcular precio en base a precio unitario
  $("#preciounitario").bind('change', function(event) {
    event.preventDefault();
    var precio = parseFloat($("#preciounitario").val()).toFixed(2);
    if (precio > 0) {
      var cant = parseFloat($("#cantidad").val());
      $("#preciototal").val((precio*cant).toFixed(2));
    }
  });

  // agregar item
  $("#agregaritem").click(function(event) {
    event.preventDefault();
    var cantidad = $("#cantidad").val(),
    precio = parseFloat($("#preciounitario").val()).toFixed(2),
    preciototal = parseFloat($("#preciototal").val()).toFixed(2),
    id = $("#id_hidden").val();

    if ($("#q").val() != ""){
      var producto = $("#q").val();
    } else {
      var producto = $("#qbc-name").val();
    }

    if (producto != "" && precio > 0 && preciototal > 0) {
      newrow = "<tr>";
      newrow = newrow + "<td class='td_nombre'>" + producto + "</td>";
      newrow = newrow + "<td class='td_precio'>$" + precio + "</td>";
      newrow = newrow + "<td class='td_cantidad'>" + cantidad + "</td>";
      newrow = newrow + "<td class='item_preciototal'>$" + preciototal + "</td>";
      newrow = newrow + "<td><a href='#' class='item_remove'><i class='foundicon-remove'></i></a></td>";
      newrow = newrow + "<td class='item_id'>" + id + "</td>";
      newrow = newrow + "</tr>";

      $('#items').find('tbody').append(newrow);

      updateTotal();
      rebindRemove();

      $("#limpiaritem").click();

    }
  });

  $( "#pagacon" ).keyup(function(e) {
    var vuelto = 0;
    var preciototal = $("#total_findeventa").text().replace('$', '');
    var precionum = parseFloat(preciototal);
    var pagacon_ = $(this).val();
    var pagacon = parseFloat(pagacon_);

    vuelto =  pagacon - precionum;

    if (vuelto > 0) {
      $('#vuelto').text(vuelto.toFixed(2));
    } else {
      $('#vuelto').text("-");
    }
    if (e.keyCode == 13){
      $('#aceptarventa').click();
    }
  });

  $(document).on('opened', '[data-reveal]', function () {
    var modal = $(this);
    $( "#pagacon" ).focus();
  });


  var no_procesando = true;

  $('#aceptarventa').click(function(e) {
    e.preventDefault();
    if (no_procesando){
      //No se está procesando
      no_procesando = false;
      $('#aceptarventa').hide();
      var efectivo = '';
      if ($("#checkbox1").is(":checked")) efectivo = 'efectivo';
      if ($("#checkbox2").is(":checked")) efectivo = 'cuentacorriente';
      if ($("#checkbox3").is(":checked")) efectivo = 'debito';
      if ($("#checkbox4").is(":checked")) efectivo = 'credito';
      if ($("#checkbox5").is(":checked")) efectivo = 'cheque';
      $.ajax({
        type: "POST",
        url: ipsiteurl + "libreria/venta/procesar_venta",
        data: {
          observaciones: $('textarea#observaciones').val(),
          total: $("#total_findeventa").text().replace('$', ''),
          pago_con: $('#pagacon').val(),
          vuelto: $('#vuelto').text(),
          cuenta_corriente: $('#cliente option:selected').val(),
          efectivo: efectivo,
          post: 1
        }
      })
        .done(function(msg) {
          console.log("success:" + msg);

          var _id_venta = msg;

          $('#items tbody tr').each(function(index, el) {

            var _id_producto = $(this).children('td.item_id').text();
            var _cantidad = $(this).children('td.td_cantidad').text();
            var _precio_unitario = $(this).children('td.td_precio').text();

            $.ajax({
              type: "POST",
              url: ipsiteurl + "libreria/venta/procesar_venta_detalle",
              data: {
                id_venta: _id_venta,
                id_producto: _id_producto,
                cantidad: _cantidad,
                precio_unitario: _precio_unitario,
                post: 1
              }
            })
            .done(function(msg) {
              //console.log(msg);
            })
            .fail(function() {
              //console.log("error");
            })
            .always(function() {
              //console.log("complete");
            });

          });
          window.location.href = ipsiteurl + 'libreria/venta/ingresar';
        })
        .fail(function() {
          //console.log("error");
        })
        .always(function() {
          //console.log("complete");
        });
      }
    });

  $('#findeventa-link').click(function(event) {
    updateTotal();
    var rowCount = $('#items tr').length;
    //console.log();
    if (rowCount > 1) {
      $('#findeventa').foundation('reveal', 'open');
    }else{
      alert("No se puede cargar una venta vacía.");
    }
  });

  $('#qbc').focus();

});
